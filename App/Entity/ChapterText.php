<?php

namespace App\Entity;

/**
 * ChapterText
 */
class ChapterText
{
    /**
     * @var integer
     */
    private $chapterText_id;

    /**
     * @var integer
     */
    private $chapter_id;

    /**
     * @var \DateTime
     */
    private $logtime;

    /**
     * @var string
     */
    private $content;

    /**
     * @var integer
     */
    private $autoSave;

    /**
     * @var integer
     */
    private $active;

    /**
     * @var integer
     */
    private $user_id;


    /**
     * Get chapterTextId
     *
     * @return integer
     */
    public function getChapterTextId()
    {
        return $this->chapterText_id;
    }

    /**
     * Set chapterId
     *
     * @param integer $chapterId
     *
     * @return ChapterText
     */
    public function setChapterId($chapterId)
    {
        $this->chapter_id = $chapterId;

        return $this;
    }

    /**
     * Get chapterId
     *
     * @return integer
     */
    public function getChapterId()
    {
        return $this->chapter_id;
    }

    /**
     * Set logtime
     *
     * @param \DateTime $logtime
     *
     * @return ChapterText
     */
    public function setLogtime($logtime)
    {
        $this->logtime = $logtime;

        return $this;
    }

    /**
     * Get logtime
     *
     * @return \DateTime
     */
    public function getLogtime()
    {
        return $this->logtime;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return ChapterText
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set autoSave
     *
     * @param integer $autoSave
     *
     * @return ChapterText
     */
    public function setAutoSave($autoSave)
    {
        $this->autoSave = $autoSave;

        return $this;
    }

    /**
     * Get autoSave
     *
     * @return integer
     */
    public function getAutoSave()
    {
        return $this->autoSave;
    }

    /**
     * Set active
     *
     * @param integer $active
     *
     * @return ChapterText
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return ChapterText
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }
}
