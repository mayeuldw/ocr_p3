<?php

namespace App\Entity;

/**
 * Right
 */
class Right
{
    /**
     * @var integer
     */
    private $right_id;

    /**
     * @var integer
     */
    private $rightLevel_id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $state;


    /**
     * Get rightId
     *
     * @return integer
     */
    public function getRightId()
    {
        return $this->right_id;
    }

    /**
     * Set rightLevelId
     *
     * @param integer $rightLevelId
     *
     * @return Right
     */
    public function setRightLevelId($rightLevelId)
    {
        $this->rightLevel_id = $rightLevelId;

        return $this;
    }

    /**
     * Get rightLevelId
     *
     * @return integer
     */
    public function getRightLevelId()
    {
        return $this->rightLevel_id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Right
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set state
     *
     * @param integer $state
     *
     * @return Right
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer
     */
    public function getState()
    {
        return $this->state;
    }
}
