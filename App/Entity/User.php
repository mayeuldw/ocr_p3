<?php

namespace App\Entity;

/**
 * User
 */
class User
{
    /**
     * @var integer
     */
    private $user_id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $mail;

    /**
     * @var string
     */
    private $passwordh;

    /**
     * @var boolean
     */
    private $newsletter;

    /**
     * @var integer
     */
    private $rightLevel_id;

    /**
     * @var boolean
     */
    private $passwordexpired;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $books;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->books = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set mail
     *
     * @param string $mail
     *
     * @return User
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set passwordh
     *
     * @param string $passwordh
     *
     * @return User
     */
    public function setPasswordh($passwordh)
    {
        $this->passwordh = $passwordh;

        return $this;
    }

    /**
     * Get passwordh
     *
     * @return string
     */
    public function getPasswordh()
    {
        return $this->passwordh;
    }

    /**
     * Set newsletter
     *
     * @param boolean $newsletter
     *
     * @return User
     */
    public function setNewsletter($newsletter)
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    /**
     * Get newsletter
     *
     * @return boolean
     */
    public function getNewsletter()
    {
        return $this->newsletter;
    }

    /**
     * Set rightLevelId
     *
     * @param integer $rightLevelId
     *
     * @return User
     */
    public function setRightLevelId($rightLevelId)
    {
        $this->rightLevel_id = $rightLevelId;

        return $this;
    }

    /**
     * Get rightLevelId
     *
     * @return integer
     */
    public function getRightLevelId()
    {
        return $this->rightLevel_id;
    }

    /**
     * Set passwordexpired
     *
     * @param boolean $passwordexpired
     *
     * @return User
     */
    public function setPasswordexpired($passwordexpired)
    {
        $this->passwordexpired = $passwordexpired;

        return $this;
    }

    /**
     * Get passwordexpired
     *
     * @return boolean
     */
    public function getPasswordexpired()
    {
        return $this->passwordexpired;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return User
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Add book
     *
     * @param \App\Entity\Book $book
     *
     * @return User
     */
    public function addBook(\App\Entity\Book $book)
    {
        $this->books[] = $book;

        return $this;
    }

    /**
     * Remove book
     *
     * @param \App\Entity\Book $book
     */
    public function removeBook(\App\Entity\Book $book)
    {
        $this->books->removeElement($book);
    }

    /**
     * Get books
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBooks()
    {
        return $this->books;
    }
}
