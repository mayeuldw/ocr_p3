<?php

namespace Framework\Entity;

/**
 * Book
 */
class Book
{
    /**
     * @var integer
     */
    private $book_id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $commentary = '';

    /**
     * @var boolean
     */
    private $public = true;


    /**
     * Get bookId
     *
     * @return integer
     */
    public function getBookId()
    {
        return $this->book_id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Book
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set commentary
     *
     * @param string $commentary
     *
     * @return Book
     */
    public function setCommentary($commentary)
    {
        $this->commentary = $commentary;

        return $this;
    }

    /**
     * Get commentary
     *
     * @return string
     */
    public function getCommentary()
    {
        return $this->commentary;
    }

    /**
     * Set public
     *
     * @param boolean $public
     *
     * @return Book
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * Get public
     *
     * @return boolean
     */
    public function getPublic()
    {
        return $this->public;
    }
}
