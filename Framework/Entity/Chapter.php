<?php

namespace Framework\Entity;

/**
 * Chapter
 */
class Chapter
{
    /**
     * @var integer
     */
    private $chapter_id;

    /**
     * @var integer
     */
    private $book_id;

    /**
     * @var integer
     */
    private $num_order = 1;

    /**
     * @var string
     */
    private $title;

    /**
     * @var integer
     */
    private $public = 0;

    /**
     * @var \DateTime
     */
    private $publication_date = '2018-01-01 00:00:00';


    /**
     * Get chapterId
     *
     * @return integer
     */
    public function getChapterId()
    {
        return $this->chapter_id;
    }

    /**
     * Set bookId
     *
     * @param integer $bookId
     *
     * @return Chapter
     */
    public function setBookId($bookId)
    {
        $this->book_id = $bookId;

        return $this;
    }

    /**
     * Get bookId
     *
     * @return integer
     */
    public function getBookId()
    {
        return $this->book_id;
    }

    /**
     * Set numOrder
     *
     * @param integer $numOrder
     *
     * @return Chapter
     */
    public function setNumOrder($numOrder)
    {
        $this->num_order = $numOrder;

        return $this;
    }

    /**
     * Get numOrder
     *
     * @return integer
     */
    public function getNumOrder()
    {
        return $this->num_order;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Chapter
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set public
     *
     * @param integer $public
     *
     * @return Chapter
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * Get public
     *
     * @return integer
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * Set publicationDate
     *
     * @param \DateTime $publicationDate
     *
     * @return Chapter
     */
    public function setPublicationDate($publicationDate)
    {
        $this->publication_date = $publicationDate;

        return $this;
    }

    /**
     * Get publicationDate
     *
     * @return \DateTime
     */
    public function getPublicationDate()
    {
        return $this->publication_date;
    }
}
