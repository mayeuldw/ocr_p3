<?php

namespace Framework\Entity;

/**
 * ChapterCommentary
 */
class ChapterCommentary
{
    /**
     * @var integer
     */
    private $chapterCommentary_id;

    /**
     * @var integer
     */
    private $chapter_id;

    /**
     * @var \DateTime
     */
    private $logtime;

    /**
     * @var integer
     */
    private $responseTo;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $content;

    /**
     * @var integer
     */
    private $state;

    /**
     * @var integer
     */
    private $user_id;


    /**
     * Get chapterCommentaryId
     *
     * @return integer
     */
    
    function __construct() {
       error_log('???');
    }
    
    public function getChapterCommentaryId()
    {
        return $this->chapterCommentary_id;
    }

    /**
     * Set chapterId
     *
     * @param integer $chapterId
     *
     * @return ChapterCommentary
     */
    public function setChapterId($chapterId)
    {
        $this->chapter_id = $chapterId;

        return $this;
    }

    /**
     * Get chapterId
     *
     * @return integer
     */
    public function getChapterId()
    {
        return $this->chapter_id;
    }

    /**
     * Set logtime
     *
     * @param \DateTime $logtime
     *
     * @return ChapterCommentary
     */
    public function setLogtime($logtime)
    {
        $this->logtime = $logtime;

        return $this;
    }

    /**
     * Get logtime
     *
     * @return \DateTime
     */
    public function getLogtime()
    {
        return $this->logtime;
    }

    /**
     * Set responseTo
     *
     * @param integer $responseTo
     *
     * @return ChapterCommentary
     */
    public function setResponseTo($responseTo)
    {
        $this->responseTo = $responseTo;

        return $this;
    }

    /**
     * Get responseTo
     *
     * @return integer
     */
    public function getResponseTo()
    {
        return $this->responseTo;
    }

    /**
     * Set login
     *
     * @param string $login
     *
     * @return ChapterCommentary
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ChapterCommentary
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return ChapterCommentary
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set state
     *
     * @param integer $state
     *
     * @return ChapterCommentary
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return ChapterCommentary
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }
}
