<?php

namespace Framework\Entity;

/**
 * RightLevel
 */
class RightLevel
{
    /**
     * @var integer
     */
    private $rightLevel_id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var boolean
     */
    private $visible;

    /**
     * @var integer
     */
    private $state;


    /**
     * Get rightLevelId
     *
     * @return integer
     */
    public function getRightLevelId()
    {
        return $this->rightLevel_id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return RightLevel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     *
     * @return RightLevel
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set state
     *
     * @param integer $state
     *
     * @return RightLevel
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer
     */
    public function getState()
    {
        return $this->state;
    }
}
