<?php

    $entityManager = require_once __DIR__.'/src/Framework/doctrine_config.php';
    use Doctrine\ORM\Tools\Console\ConsoleRunner;
    return ConsoleRunner::createHelperSet($entityManager);