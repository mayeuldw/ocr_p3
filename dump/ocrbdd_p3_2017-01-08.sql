-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Client :  192.168.53.45
-- Généré le :  Lun 08 Janvier 2018 à 10:13
-- Version du serveur :  10.0.32-MariaDB-0+deb8u1
-- Version de PHP :  5.6.29-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `ocrbdd_p3`
--

-- --------------------------------------------------------

--
-- Structure de la table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
`book_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `commentary` longtext COLLATE utf8_unicode_ci NOT NULL,
  `public` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `books`
--

INSERT INTO `books` (`book_id`, `name`, `commentary`, `public`) VALUES
(1, 'Billet simple pour l''Alaska', '', 1);

-- --------------------------------------------------------

--
-- Structure de la table `booksUsers`
--

CREATE TABLE IF NOT EXISTS `booksUsers` (
  `user_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `booksUsers`
--

INSERT INTO `booksUsers` (`user_id`, `book_id`) VALUES
(1, 1),
(2, 1),
(4, 1);

-- --------------------------------------------------------

--
-- Structure de la table `chapterCommentaries`
--

CREATE TABLE IF NOT EXISTS `chapterCommentaries` (
`chapterCommentary_id` int(11) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `logtime` datetime NOT NULL,
  `responseTo` int(11) NOT NULL,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `chapterCommentaries`
--

INSERT INTO `chapterCommentaries` (`chapterCommentary_id`, `chapter_id`, `logtime`, `responseTo`, `login`, `title`, `content`, `state`, `user_id`) VALUES
(1, 1, '2018-01-05 09:03:48', 0, 'Mayeul', 'test de  commentaire', 'mon commentaire', 0, 1),
(2, 1, '2018-01-05 09:04:04', 1, 'Mayeul', 'test de  commentaire', 'mon commentaire', 3, 1),
(5, 3, '2018-01-05 16:40:26', 0, 'mc lodge', 'super début', 'vraiment très bien, continue ...', 2, NULL),
(27, 3, '2018-01-08 09:02:14', 0, 'Mayeul', 'test', 'test test', 1, NULL),
(28, 3, '2018-01-08 09:05:12', 0, '&lt;p style:&quot;color:red&quot;&gt;Bonjour&lt;/p&gt;', 'test', 'td', 1, NULL),
(29, 3, '2018-01-08 09:05:34', 0, '<p style:"color:red">Bonjour</p>', 'test', 'test', 1, NULL),
(30, 3, '2018-01-08 09:05:49', 0, '<p style="color:red">Bonjour</p>', 'Tes', 'test', 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `chapters`
--

CREATE TABLE IF NOT EXISTS `chapters` (
`chapter_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `num_order` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `public` int(11) NOT NULL,
  `publication_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `chapters`
--

INSERT INTO `chapters` (`chapter_id`, `book_id`, `num_order`, `title`, `public`, `publication_date`) VALUES
(1, 1, 1, 'Chapitre 1', 1, NULL),
(2, 1, 2, 'Chapitre 2', 1, NULL),
(3, 1, 3, 'Chapitre3', 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `chapterTexts`
--

CREATE TABLE IF NOT EXISTS `chapterTexts` (
`chapterText_id` int(11) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `logtime` datetime NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `autoSave` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `chapterTexts`
--

INSERT INTO `chapterTexts` (`chapterText_id`, `chapter_id`, `logtime`, `content`, `autoSave`, `active`, `user_id`) VALUES
(1, 1, '2018-01-05 09:03:25', '<p>mon texte a moi</p>', 0, 1, 1),
(2, 3, '2018-01-05 16:39:57', '<p>il &eacute;tait une fois :-)</p>', 0, 1, 4);

-- --------------------------------------------------------

--
-- Structure de la table `rightLevels`
--

CREATE TABLE IF NOT EXISTS `rightLevels` (
`rightLevel_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `state` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `rightLevels`
--

INSERT INTO `rightLevels` (`rightLevel_id`, `name`, `visible`, `state`) VALUES
(1, 'Administrateur', 1, 0),
(2, 'Editeur', 1, 1),
(3, 'Modérateur', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `rights`
--

CREATE TABLE IF NOT EXISTS `rights` (
`right_id` int(11) NOT NULL,
  `rightLevel_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `rights`
--

INSERT INTO `rights` (`right_id`, `rightLevel_id`, `name`) VALUES
(1, 1, 'updateRight'),
(2, 1, 'CreateUser'),
(3, 1, 'UpdateUser'),
(4, 1, 'ResetPasswordUser'),
(5, 2, 'SeeChapterList'),
(6, 2, 'CreateChapter'),
(7, 2, 'UpdateChapter'),
(8, 2, 'deleteCommentary'),
(9, 3, 'deleteCommentary'),
(10, 2, 'moderateCommentary'),
(11, 3, 'moderateCommentary'),
(12, 2, 'seeCommentary'),
(13, 3, 'seeCommentary'),
(14, 1, 'seeUser');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `passwordh` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `newsletter` tinyint(1) NOT NULL,
  `rightLevel_id` int(11) NOT NULL,
  `passwordexpired` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`user_id`, `name`, `firstName`, `mail`, `passwordh`, `newsletter`, `rightLevel_id`, `passwordexpired`, `active`) VALUES
(1, 'de WERBIER', 'Mayeul', 'mayeul.dewerbier@delta-group.fr', 'P2NA1ZKJeAqHbLl5yUvk4mE+mqw8yw8q7ehV2cfkwr4=', 0, 1, 0, 1),
(2, 'de WERBIER', 'Mayeul (2)', 'mayeul.dewerbier@dev-tadeo.fr', 'y7/vT9Tf1+aSg3OJZRadD9/hvSx0sE8899Jx/5zgQzE=', 0, 2, 0, 1),
(4, 'Benoit', 'Laude', 'benoit_laude@yahoo.fr', 'OBxvcSIi5YJEjE/r19JUx/bxW9zDfJe8wfo53FCGHwc=', 0, 1, 0, 1);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `books`
--
ALTER TABLE `books`
 ADD PRIMARY KEY (`book_id`);

--
-- Index pour la table `booksUsers`
--
ALTER TABLE `booksUsers`
 ADD PRIMARY KEY (`user_id`,`book_id`), ADD KEY `IDX_ABEE5AD9A76ED395` (`user_id`), ADD KEY `IDX_ABEE5AD916A2B381` (`book_id`);

--
-- Index pour la table `chapterCommentaries`
--
ALTER TABLE `chapterCommentaries`
 ADD PRIMARY KEY (`chapterCommentary_id`);

--
-- Index pour la table `chapters`
--
ALTER TABLE `chapters`
 ADD PRIMARY KEY (`chapter_id`);

--
-- Index pour la table `chapterTexts`
--
ALTER TABLE `chapterTexts`
 ADD PRIMARY KEY (`chapterText_id`);

--
-- Index pour la table `rightLevels`
--
ALTER TABLE `rightLevels`
 ADD PRIMARY KEY (`rightLevel_id`);

--
-- Index pour la table `rights`
--
ALTER TABLE `rights`
 ADD PRIMARY KEY (`right_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `books`
--
ALTER TABLE `books`
MODIFY `book_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `chapterCommentaries`
--
ALTER TABLE `chapterCommentaries`
MODIFY `chapterCommentary_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT pour la table `chapters`
--
ALTER TABLE `chapters`
MODIFY `chapter_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `chapterTexts`
--
ALTER TABLE `chapterTexts`
MODIFY `chapterText_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `rightLevels`
--
ALTER TABLE `rightLevels`
MODIFY `rightLevel_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `rights`
--
ALTER TABLE `rights`
MODIFY `right_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `booksUsers`
--
ALTER TABLE `booksUsers`
ADD CONSTRAINT `FK_ABEE5AD916A2B381` FOREIGN KEY (`book_id`) REFERENCES `books` (`book_id`),
ADD CONSTRAINT `FK_ABEE5AD9A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
