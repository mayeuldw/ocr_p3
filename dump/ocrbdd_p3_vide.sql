-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Client :  192.168.53.45
-- Généré le :  Mer 10 Janvier 2018 à 10:32
-- Version du serveur :  10.0.32-MariaDB-0+deb8u1
-- Version de PHP :  5.6.29-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `ocrbdd_p3`
--

-- --------------------------------------------------------

--
-- Structure de la table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
`book_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `commentary` longtext COLLATE utf8_unicode_ci NOT NULL,
  `public` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `booksUsers`
--

CREATE TABLE IF NOT EXISTS `booksUsers` (
  `user_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `chapterCommentaries`
--

CREATE TABLE IF NOT EXISTS `chapterCommentaries` (
`chapterCommentary_id` int(11) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `logtime` datetime NOT NULL,
  `responseTo` int(11) NOT NULL,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `chapterTexts`
--

CREATE TABLE IF NOT EXISTS `chapterTexts` (
`chapterText_id` int(11) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `logtime` datetime NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `autoSave` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `rightLevels`
--

CREATE TABLE IF NOT EXISTS `rightLevels` (
`rightLevel_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `state` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `rights`
--

CREATE TABLE IF NOT EXISTS `rights` (
`right_id` int(11) NOT NULL,
  `rightLevel_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `passwordh` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `newsletter` tinyint(1) NOT NULL,
  `rightLevel_id` int(11) NOT NULL,
  `passwordexpired` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `books`
--
ALTER TABLE `books`
 ADD PRIMARY KEY (`book_id`);

--
-- Index pour la table `booksUsers`
--
ALTER TABLE `booksUsers`
 ADD PRIMARY KEY (`user_id`,`book_id`), ADD KEY `IDX_ABEE5AD9A76ED395` (`user_id`), ADD KEY `IDX_ABEE5AD916A2B381` (`book_id`);

--
-- Index pour la table `chapterCommentaries`
--
ALTER TABLE `chapterCommentaries`
 ADD PRIMARY KEY (`chapterCommentary_id`);

--
-- Index pour la table `chapterTexts`
--
ALTER TABLE `chapterTexts`
 ADD PRIMARY KEY (`chapterText_id`);

--
-- Index pour la table `rightLevels`
--
ALTER TABLE `rightLevels`
 ADD PRIMARY KEY (`rightLevel_id`);

--
-- Index pour la table `rights`
--
ALTER TABLE `rights`
 ADD PRIMARY KEY (`right_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `books`
--
ALTER TABLE `books`
MODIFY `book_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `chapterCommentaries`
--
ALTER TABLE `chapterCommentaries`
MODIFY `chapterCommentary_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `chapterTexts`
--
ALTER TABLE `chapterTexts`
MODIFY `chapterText_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rightLevels`
--
ALTER TABLE `rightLevels`
MODIFY `rightLevel_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rights`
--
ALTER TABLE `rights`
MODIFY `right_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `booksUsers`
--
ALTER TABLE `booksUsers`
ADD CONSTRAINT `FK_ABEE5AD916A2B381` FOREIGN KEY (`book_id`) REFERENCES `books` (`book_id`),
ADD CONSTRAINT `FK_ABEE5AD9A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
