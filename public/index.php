<?php
    session_start();
    session_set_cookie_params(604800); // 7j
    if($_SERVER['REQUEST_URI'] === '/favicon.ico') exit();
    if($_SERVER['REQUEST_URI'] === '/purify.min.js.map') exit();
    if($_SERVER['REQUEST_URI'] === '/dist/purify.min.js.map') exit();
    error_log('#'.$_SERVER['REQUEST_URI'].'#');
    if(count($_POST) > 0) error_log(print_r($_POST,true));
    

    error_reporting(E_ALL &~ E_DEPRECATED);
    ini_set('display_errors', 'on'); 

    require '../vendor/autoload.php';
    
    $app = new \Framework\App([
        'PrivateSite',
        'PublicSite'
        ], $_SESSION);
    $response = $app->run(\GuzzleHttp\Psr7\ServerRequest::fromGlobals());
    \Http\Response\send($response);
    $_SESSION = $app->getBackSession();
    session_write_close();
    