<?php
namespace Framework\Aes;

class AES {
    public static function Encrypt($key, $msg) {
        $iv     = openssl_random_pseudo_bytes(16);
        $data   = openssl_encrypt ($msg, 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv);
        return $iv.$data;
    }

    public static function Decrypt($key, $msg) {
        $iv     = substr($msg,0,16);
        $data   = substr($msg,16);
        $clear  = openssl_decrypt ($data, 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv);
        return $clear;
    }
}
