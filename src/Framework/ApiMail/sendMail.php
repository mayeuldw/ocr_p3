<?php

    include 'sub/class.phpmailer.php';
    include 'sub/class.smtp.php';
    $arg1 = (json_decode(base64_decode($argv[1]),true));
    
    $mail = new PHPMailer();                                    
    $mail->IsSMTP();                                            
    $mail->SMTPAuth     = $arg1['settings']['smtpAuth'];              
    $mail->Host         = $arg1['settings']['host'];             
    $mail->Port         = $arg1['settings']['port'];               
    $mail->Username     = $arg1['settings']['userName'];            
    $mail->Password     = $arg1['settings']['password'];             
    $mail->Sender	= $arg1['settings']['userName'];              
    $mail->From         = $arg1['settings']['userName'];            
    $mail->FromName     = $arg1['settings']['fromName'];                
    $mail->Subject      = $arg1['object'];
    $mail->AddAddress($arg1['to']);
    $mail->MsgHTML($arg1['text']);
    $mail->Send();
