<?php
namespace Framework;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class App
{
    protected $session;
    protected $controller;
    protected $router;

    function __construct(array $modules = [], $session = []) {
        $this->session = $session;
        $this->router = new Router();
        $this->router->makeRoutes($modules,  __DIR__.'/../');
    }
    public function run(ServerRequestInterface $request) : ResponseInterface
    {
        $defaultPublicview = '/404';
        $defaultPrivateview = '/login';
        
        // Match Route, or '/404' if not exist
        $route = $this->router->match($request, $defaultPublicview);
        $json = strpos($route->getName(),'.json') !== false;
        $params = $route->getParams();
        $request = array_reduce(array_keys($params), function ($request, $key) use ($params){
            return $request->withAttribute($key, $params[$key]);
        }, $request);
        $this->runController($route, $this->router, $request);
        if(strpos($route->getName(),'public.') !== false)
        {
            // Get Back Session
            $this->session = $this->controller->getBackSession();
            $route = $this->router->matchString($this->session['PreviousAdminView'] ?? $defaultPrivateview, $defaultPrivateview);
            $this->runController($route, $this->router, $request, $this->controller->getBackNotify());
        }
        else
        {
            $_r1 = $this->router->matchString($request->getServerParams()['REQUEST_URI'], '');
            $_r2 = $this->router->matchStringPOST($request->getServerParams()['REQUEST_URI'], '');
            if($_r1 === null && $_r2 === null)
                exit('bye');
        }

        // Run Whole Renderer
        $response = $this->controller->renderer($json);
        return $response;
    }
    
    private function runController($route, Router $router, ServerRequestInterface $request, array $notify = [])
    {
        if($route === null) return false;
        $item = ($route->getCallback())();
        $class = $item['nameSpace'].'\\'.$item['controller'];
        $this->controller = new $class($this->session, $router, $notify);
        $this->controller->{$item['functionName']}($request);
    }


    public function getBackSession() : Array
    {
        return $this->controller->getBackSession();
    }
}
