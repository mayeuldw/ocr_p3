<?php

namespace App\PublicSite\Manager;

namespace Framework\Controller;

use Psr\Http\Message\ServerRequestInterface;

use Defuse\Crypto\Key;
use Defuse\Crypto\Crypto;

use Framework\Entity\Book;

class AdminController extends Controller
{
    protected function isLogged(ServerRequestInterface $request) : bool
    {
        $userManager = new \Framework\Manager\UserFrameworkRight();
        $user = $userManager->get($this->session['user_id'] ?? null);
        $this->ctrl_data['user'] = $user;
        if($user === null)
        {
            $this->ctrl_data['userName'] = null;
            $this->rendererLoginView();
            $this->publicView();
            return false;
        }
        if($user->getPasswordexpired() == 1)
        {
            $this->ctrl_data['userName'] = null;
            $this->ctrl_data['show'] = 'show';
            $this->rendererExpiredPasswordView($user->getId());
            $this->publicView();
            return false;
        }
        $this->ctrl_data['userName'] = $user->getFirstName().' '.$user->getName();
        return true;
    }
    
    function changePasswordSelf(ServerRequestInterface $request)
    {
        if($this->isLogged($request))
        {
            $this->ctrl_data['userName'] = null;
            $this->rendererExpiredPasswordView($this->ctrl_data['user']->getId(), false);
        }
    }
    function askNewPwd(ServerRequestInterface $request)
    {
            $this->rendererAskPasswordView();
    }
    
    protected function isAllowed(ServerRequestInterface $request, string $RightName) : bool
    {

        if($this->isLogged($request))
        {
            if($this->checkRight($RightName)) return true;
            $this->setNotify('Vous ne disposez pas des doits nécessaire pour effectuer cette action.', 'danger', 'fa fa-user');
        }
        return false;
    }
    
    protected function bookIsAllowed(Book $book) : bool
    {
        $userManager = new \Framework\Manager\UserFrameworkRight();
        return $userManager->checkBookRight($this->ctrl_data['user'], $book);
    }
    
    

    protected function checkRight(string $RightName)
    {
        
        if($this->ctrl_data['user'] === null) return false;
        $userManager = new \Framework\Manager\UserFrameworkRight();
        return $userManager->checkRight($this->ctrl_data['user'], $RightName);
    }



    protected function rendererAdminDefaultView(ServerRequestInterface $request)
    {
        $this->rendererLoginView();
        $this->publicView();
    }
    
    public function logIn(ServerRequestInterface $request)
    {
        if($this->isLogged($request))
        {
            $this->rendererAdminDefaultView($request);
            $this->publicView();
        }
    }
    
    public function doLogIn(ServerRequestInterface $request)
    {
        $login = $request->getParsedBody()['login'];
        $pwd =   $request->getParsedBody()['pwd'];
        if($login === '') return $this->setNotify('L\'identifiant de connexion ne peut etre vide.', 'danger', 'fa fa-close');
        
        $userManager = new \Framework\Manager\UserFrameworkRight();
        $user = $userManager->logUser($login, $pwd);
        if($user === null)
        {
            $this->ctrl_data['userName'] = null;
            return $this->setNotify('L\'autentification à echouée.', 'danger', 'fa fa-close');
        }
        $this->setNotify('Bienvenu '.$user->getFirstName());
        $this->ctrl_data['show'] = 'hide';
        $this->session['user_id'] = $user->getid();

        if($this->isLogged($request)) $this->rendererAdminDefaultView($request);
    }
    
    public function doLogOut(ServerRequestInterface $request)
    {
        $this->session['user_id'] = null;
        $this->ctrl_data['show'] = 'hide';
        $this->setNotify('Vous avez été déconnecté.');

        if($this->isLogged($request)) $this->rendererAdminDefaultView($request);
    }
    
    // See PublicSite
    protected function publicView()
    {
        return false;
    }
}
