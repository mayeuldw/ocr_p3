<?php

namespace Framework\Controller;

use GuzzleHttp\Psr7\Response;
use Framework\Manager\NotifyManager;

class Controller
{
    private $mailSettings = [
        'userName'  => 'ocr.mdewerbier@gmail.com',
        'fromName'  => 'Billet simple pour l\'Alaska',
        'password'  => '49A89Qb2ZqSf2',
        'host'      => 'ssl://smtp.gmail.com:465',
        'port'      => 465,
        'smtpAuth'  => true
    ];
    
    
    private $namePublicDiv = 'publicSite';
    private $namePrivateDiv = 'privateSite';
    protected $ctrl_data = [
        'userName' => null,
        'hasRightToEditCurrentChapter' => false,
        'hasRightToToModerate' => false,
        'editMode' => false
    ];
    
    private $data = [
        'divRenderer'=>[],
        'notify'=>[],
        'action'=>'none'
    ];
    private $layout = 'HTML';
    protected $session;
    protected $router;
    
    private $notifyMng;
    
    function __construct($session, $router, $notify = []) {
        $this->session = $session;
        $this->router = $router;
        $this->set_divRenderer($this->namePublicDiv, '');
        $this->set_divRenderer($this->namePrivateDiv, '');
        
        $this->notifyMng = new NotifyManager();
        
        foreach($notify as $n)
            $this->data['notify'][] = $n;
    }
    
    private function set_divRenderer($div_id, $content)
    {
        $this->data['divRenderer'][$div_id] = $content;
        return true;
    }
    
    
    protected function rendererSelectedDiv(string $div_id, string $view, array $data =[])
    {
        extract($data);
        ob_start();
        require $view.'.php';
        $content = ob_get_clean();
        if($div_id === '') return $content;
        
        $this->set_divRenderer($div_id, $content);
        return true;
    }
    
    protected function rendererAdminView(string $view, array $data =[])
    {
        $to_private_view = [
            'content' => $this->rendererSelectedDiv('', $view, $data),
            'router'  => $data['router'],
            'route_name'  => $data['route_name'],
            'displayUserBtn' => $this->checkRight('seeUser')
            
        ];
        extract($to_private_view);
        ob_start();
        require __DIR__.'/../Layout/private_site.php';
        $this->data['divRenderer'][$this->namePrivateDiv] = ob_get_clean();
        return true;
    }
    
    protected function rendererLoginView()
    {
        ob_start();
        require __DIR__.'/../Layout/private_login.php';
        $this->data['divRenderer'][$this->namePrivateDiv] = ob_get_clean();
        return true;
    }
    
    protected function rendererExpiredPasswordView(int $user_id, $expired = true)
    {
        extract(['user_id'=> $user_id, 'expired'=>$expired, 'router'=> $this->router]);
        ob_start();
        require __DIR__.'/../Layout/private_changepwd.php';
        $this->data['divRenderer'][$this->namePrivateDiv] = ob_get_clean();
        return true;
    }
    
    protected function rendererAskPasswordView()
    {
        extract(['router'=> $this->router]);
        ob_start();
        require __DIR__.'/../Layout/private_asknewpwd.php';
        $this->data['divRenderer'][$this->namePrivateDiv] = ob_get_clean();
        return true;
    }

        protected function setNotify(string $msg, string $type = 'success', string $ico = 'fa fa-check')
    {
        $this->data['notify'][] = $this->notifyMng->newNotify($msg, $type, $ico);
    }
    
    protected function rendererPublicView(string $view, array $data = [])
    {
        return $this->rendererSelectedDiv($this->namePublicDiv, $view, $data);
    }
    
    public function sendMail(string $email, string $title, string $layout, array $data)
    {
        extract($data);
        ob_start();
        require __DIR__.'/../Layout/'.$layout.'.php';
        $mail = base64_encode(json_encode([
            'settings'  => $this->mailSettings,
            'to'        => $email,
            'object'    => $title,
            'text'      => ob_get_clean()
        ]));
      
        pclose(popen('php '.__DIR__.'/../ApiMail/sendMail.php '.$mail.' &','r'));
    }
    
    protected function showAdmin()
    {
        $this->ctrl_data['show'] = 'show';
    }

    protected function hideAdmin()
    {
        $this->ctrl_data['show'] = 'hide';
    }
    
    public function renderer($json) : Response
    {
        if($json && $this->layout !== 'ForceHTML') $this->layout = 'JSON';
        switch($this->layout)
        {
            case 'JSON':
                
                $this->data['notify'] = array_map(function ($k) {return $this->notifyMng->display_json_notify($k);}, $this->data['notify']);
                $this->data['login'] = $this->ctrl_data['userName'];
                $this->data['show'] = $this->ctrl_data['show'] ?? 'none';
                $this->data['editMode'] = $this->ctrl_data['editMode'] ? 1:0;
                $renderer = [];
                foreach($this->data['divRenderer'] as $k=>$v)
                {
                    if($v !== '') $renderer[] = ['div'=>$k, 'content'=>$v];
                }
                $this->data['divRenderer'] = $renderer;
                $response = new Response(200, [], json_encode($this->data));
                // construire le JSON a renvoyer
                break;
            
            default :
                $to_view = [
                    'publicSiteDiv'  => $this->namePublicDiv,
                    'privateSiteDiv' => $this->namePrivateDiv,
                    'publicSite'     => $this->data['divRenderer'][$this->namePublicDiv] ?? '',
                    'privateSite'    => $this->data['divRenderer'][$this->namePrivateDiv] ?? '',
                    'login'          => $this->ctrl_data['userName'],
                    'moderate'       => $this->ctrl_data['hasRightToToModerate'],
                    'title'          => 'Billet simple pour l\'Alaska',
                    'show'           => $this->ctrl_data['show'] ?? 'none',
                    'editMode'       => $this->ctrl_data['editMode']
                ];
                
                $to_view['notify'] = array_map(function ($k) {return $this->notifyMng->display_web_notify($k);}, $this->data['notify']);
                $to_view['notify_function'] = $this->notifyMng->getFunctionJS();
                $to_view['notify_function_name'] = $this->notifyMng->getFunctionName();
                extract($to_view);
                ob_start();
                require __DIR__.'/../Layout/default.php';
                // go renderer whole view 
                $response = new Response(200, [], ob_get_clean());
        }
        return $response;
    }
    
    public function getBackSession() : Array
    {
        return $this->session;
    }
    public function getBackNotify() : Array
    {
        return $this->data['notify'];
    }
    
    public function debug($obj)
    {
        ob_start();
        var_dump($obj);
        error_log(ob_get_clean());
        
    }
    
    protected function checkRight(string $right)
    {
        return false;
    }
    
}
