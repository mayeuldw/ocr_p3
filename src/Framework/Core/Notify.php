<?php

namespace Framework\Core;

class Notify
{
    protected $data;
    protected $icone;
    protected $type;
    
    function __construct(string $message, string $type= 'success', string $icone = 'fa fa-check') 
    {
        $this->data = 
                [
                    'message'   => $message,
                    'icone'     => $icone,
                    'type'      => $type,
        ];
    }
    
    function get()
    {
        return $this->data;
    }
}
