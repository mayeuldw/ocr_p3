<?php

namespace Framework\Core;

class Route
{

    private $parameters;
    private $calable;
    private $name;

    public function __construct(string $name, callable $calable, array $parameters) {
        $this->name = $name;
        $this->calable = $calable;
        $this->parameters = $parameters;
    }
    
    public function getName(): string
    {
       return $this->name; 
    }
    
    public function getCallback(): callable
    {
        return $this->calable;
    }
    
    public function getParams(): array
    {
        return $this->parameters;
    }
}