<?php
namespace Framework\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(name="books")
*/
class Book
{
    /**
    * @ORM\Id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer")
    */
    protected $book_id;
    
    /**
    * @ORM\Column(type="string")
    */
    protected $name;
    
    /**
    * @ORM\Column(type="text")
    */
    protected $commentary = '';
    
    /**
    * @ORM\Column(type="boolean")
    */
    protected $public = true;
    
    public function getId() : int
    {
        return $this->book_id;
    }
    
    public function getName() : string
    {
        return $this->name;
    }
    public function setName(string $name)
    {
        $this->name = htmlspecialchars($name);
    }
    
    public function getCommentary() : string
    {
        return $this->commentary;
    }
    public function setCommentary(string $commentary)
    {
        $this->commentary = htmlspecialchars($commentary);
    }
    
    public function getPublicState() : bool
    {
        return $this->public;
    }
    public function setPublicState(bool $public)
    {
        $this->public = $public;
    }
    
    
    
}
