<?php

namespace Framework\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
* @ORM\Entity
* @ORM\Table(name="chapters")
*/
class Chapter
{
    /**
    * @ORM\Id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer")
    */
    protected $chapter_id;
    
    /**
    * @ORM\Column(type="integer")
    * @ORM\ManyToOne(targetEntity="Book")
    * @ORM\JoinColumn(name="book_id", referencedColumnName="book_id")
    */
    protected $book_id;
    
    /**
    * @ORM\Column(name="num_order", type="integer")
    */
    protected $order = 1;
    
    /**
    * @ORM\Column(type="string")
    */
    protected $title;
    
    /**
    * @ORM\Column(type="integer")
    */
    protected $public = 0;
    
    /**
    * @ORM\Column(type="datetime", nullable=true)
    */
    protected $publication_date;
    
    public function getId() : int
    {
        return $this->chapter_id;
    }
    
    public function getBookId() : int
    {
        return $this->book_id;
    }
    public function setBookId(int $book_id)
    {
        $this->book_id = $book_id;
    }
    
    public function getOrder() : int
    {
        return $this->order;
    }
    public function setOrder(int $order)
    {
        $this->order = $order;
    }
    
    public function getTitle() : string
    {
        return $this->title;
    }
    public function setTitle(string $title)
    {
        $this->title = htmlspecialchars($title);
    }
    
    public function getPublicState()
    {
        return $this->public;
    }
    public function setPublicState(string $public)
    {
        $this->public = $public;
    }
           
    public function getPublicationDate() : string
    {
        return $this->publication_date->format('Y-m-d H:i:s');
    }
    public function setPublicationDate(string $publication_date)
    {
        $this->publication_date = new DateTime($publication_date);
    }
    
}
