<?php

namespace Framework\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
* @ORM\Entity
* @ORM\Table(name="chapterCommentaries")
*/
class ChapterCommentary
{
    /**
    * @ORM\Id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer")
    */
    protected $chapterCommentary_id;
    
    /**
    * @ORM\Column(type="integer")
    * @ORM\ManyToOne(targetEntity="Chapter")
    * @ORM\JoinColumn(name="chapter_id", referencedColumnName="chapter_id")
    */
    protected $chapter_id;
    
    /**
    * @ORM\Column(type="datetime")
    */
    protected $logtime;
    
    /**
    * @ORM\Column(type="integer")
    */
    protected $responseTo;
    
    /**
    * @ORM\Column(type="string")
    */
    protected $login;
    
    /**
    * @ORM\Column(type="string")
    */
    protected $title;
     
    /**
    * @ORM\Column(type="string")
    */
    protected $content;
    
    /**
    * @ORM\Column(type="integer")
    */
    protected $state = 1;
    
    /**
    * @ORM\Column(type="integer", nullable=true))
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
    */
    protected $user_id = null;
    
   /**
     * Get chapterCommentaryId
     *
     * @return integer
     */
    public function getId()
    {
        return $this->chapterCommentary_id;
    }

    /**
     * Set chapterId
     *
     * @param integer $chapterId
     *
     * @return ChapterCommentary
     */
    public function setChapterId($chapterId)
    {
        $this->chapter_id = $chapterId;
    }

    /**
     * Get chapterId
     *
     * @return integer
     */
    public function getChapterId()
    {
        return $this->chapter_id;
    }

    /**
     * Set logtime
     *
     * @param \DateTime $logtime
     *
     * @return ChapterCommentary
     */
    public function setLogtime($logtime)
    {
        $this->logtime = new DateTime($logtime);
    }

    /**
     * Get logtime
     *
     * @return \DateTime
     */
    public function getLogtime()
    {
        return $this->logtime->format('Y-m-d H:i:s');
    }

    /**
     * Set responseTo
     *
     * @param integer $responseTo
     *
     * @return ChapterCommentary
     */
    public function setResponseTo($responseTo)
    {
        $this->responseTo = $responseTo;
    }

    /**
     * Get responseTo
     *
     * @return integer
     */
    public function getResponseTo()
    {
        return $this->responseTo;
    }

    /**
     * Set login
     *
     * @param string $login
     *
     * @return ChapterCommentary
     */
    public function setLogin($login)
    {
        $this->login = htmlspecialchars($login);
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ChapterCommentary
     */
    public function setTitle($title)
    {
        $this->title = htmlspecialchars($title);
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return ChapterCommentary
     */
    public function setContent($content)
    {
        $this->content = htmlspecialchars($content);
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set state
     *
     * @param integer $state
     *
     * @return ChapterCommentary
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * Get state
     *
     * @return integer
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return ChapterCommentary
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }
}