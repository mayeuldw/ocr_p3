<?php

namespace Framework\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
* @ORM\Entity
* @ORM\Table(name="chapterTexts")
*/
class ChapterText
{
    /**
    * @ORM\Id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer")
    */
   protected $chapterText_id;
    
    /**
    * @ORM\Column(type="integer")
    * @ORM\ManyToOne(targetEntity="Chapter")
    * @ORM\JoinColumn(name="chapter_id", referencedColumnName="chapter_id")
    */
    protected $chapter_id;
    
    /**
    * @ORM\Column(type="datetime")
    */
    protected $logtime;
    
    /**
    * @ORM\Column(type="text")
    */
    protected $content;
    
    /**
    * @ORM\Column(type="integer")
    */
    protected $autoSave = 0;

    /**
    * @ORM\Column(type="integer")
    */
    protected $active = 1;
    
    /**
    * @ORM\Column(type="integer")
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
    */
    protected $user_id;


    function getId() : int {
        return $this->chapterText_id;
    }

    function getChapter_id() : int {
        return $this->chapter_id;
    }
    function setChapter_id(int $chapter_id) {
        $this->chapter_id = $chapter_id;
    }

    function getLogtime() {
        return $this->logtime->format('Y-m-d H:i:s');
    }
    function setLogtime($logtime) {
        $this->logtime = new DateTime($logtime);
    }

    function getContent() {
        return $this->content;
    }
    function setContent($content) {
        $this->content = $content;
    }

    function getAutoSave() {
        return $this->autoSave;
    }
    function setAutoSave($autoSave) {
        $this->autoSave = $autoSave;
    }

    function getActive() {
        return $this->active;
    }
    function setActive($active) {
        $this->active = $active;
    }

    function getUser_id() {
        return $this->user_id;
    }
    function setUser_id($user_id) {
        $this->user_id = $user_id;
    }
}
