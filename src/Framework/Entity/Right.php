<?php

namespace Framework\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(name="rights")
*/
class Right
{
    /**
    * @ORM\Id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer")
    */
    protected $right_id;
    
    /**
    * @ORM\Column(type="integer")
    * @ORM\OneToMany(targetEntity="RightLevel", mappedBy="rightLevel_id")
    */
    protected $rightLevel_id;
    
    /**
    * @ORM\Column(type="string")   
    */
    protected $name;
    
    function getId() {
        return $this->right_id;
    }
    function setId($right_id) {
        $this->right_id = $right_id;
        return $this;
    }

    function getRightLevel_id() {
        return $this->rightLevel_id;
    }

    function setRightLevel_id($rightLevel_id) {
        $this->rightLevel_id = $rightLevel_id;
        return $this;
    }

    function setName($name) {
        $this->name = $name;
        return $this;
    }
    function getName() {
        return $this->name;
    }
}
