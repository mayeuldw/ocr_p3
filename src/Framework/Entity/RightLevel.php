<?php

namespace Framework\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(name="rightLevels")
*/
class RightLevel
{
    /**
    * @ORM\Id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer")
    */
    protected $rightLevel_id;
    
    /**
    * @ORM\Column(type="string")
    */
    protected $name;
    
    /**
    * @ORM\Column(type="boolean")
    */
    protected $visible;
    
    /**
    * @ORM\Column(type="integer")
    */
    protected $state;
    function getId() {
        return $this->rightLevel_id;
    }
    function setId(int $id) {
        $this->rightLevel_id = $id;
    }

    function getName() {
        return $this->name;
    }

    function getVisible() {
        return $this->visible;
    }

    function getState() {
        return $this->state;
    }

    function setName($name) {
        $this->name = $name;
        return $this;
    }

    function setVisible($visible) {
        $this->visible = $visible;
        return $this;
    }

    function setState($state) {
        $this->state = $state;
        return $this;
    }

}
