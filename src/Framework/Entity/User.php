<?php

namespace Framework\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(name="users")
*/
class User
{
    /**
    * @ORM\Id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer")
    */
    protected $user_id;
    
    /**
    * @ORM\Column(type="string")
    */
    protected $name;
    
    /**
    * @ORM\Column(type="string")
    */
    protected $firstName;
    
    /**
    * @ORM\Column(type="string")
    */
    protected $mail;
    
    /**
    * @ORM\Column(type="string")
    */
    protected $passwordh;
    
    /**
    * @ORM\Column(type="boolean")
    */
    protected $newsletter = false;
    
    /**
    * @ORM\Column(type="integer")
    * @ORM\OneToMany(targetEntity="RightLevel", mappedBy="rightLevel_id")
    */
    protected $rightLevel_id;
    
    /**
    * @ORM\Column(type="boolean")
    */
    protected $passwordexpired = true;
    
    /**
    * @ORM\Column(type="boolean")
    */
    protected $active;
    
    /**
    * @ORM\ManyToMany(targetEntity="Book")
    * @ORM\JoinTable(name="booksUsers",
    *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="user_id")},
    *      inverseJoinColumns={@ORM\JoinColumn(name="book_id", referencedColumnName="book_id", unique=false)}
    * )
    */
    protected $books;
    
    public function __construct ()
    {
        $this->books = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    function getId() {
        return $this->user_id;
    }

    function getName() {
        return $this->name;
    }
    function setName($name) {
        $this->name = htmlspecialchars($name);
    }

    function getFirstName() {
        return $this->firstName;
    }
    function setFirstName($firstName) {
        $this->firstName = htmlspecialchars($firstName);
    }

    function getMail() {
        return $this->mail;
    }
    function setMail($mail) {
        $this->mail = htmlspecialchars($mail);
    }

    private $passworkKey = '5d3c43a4ca3516c4czd3fe343a49aff67b0e0a0bfc798ffb9c0236c45fd3624f';
    
    function getPassword() {
        return \Framework\Aes\AES::Decrypt($this->passworkKey, base64_decode($this->passwordh)) ;
    }
    function setPassword($passwordh) {
        $this->passwordh = base64_encode(\Framework\Aes\AES::Encrypt($this->passworkKey, $passwordh)) ;
    }

    function getNewsletter() {
        return $this->newsletter;
    }
    function setNewsletter($newsletter) {
        $this->newsletter = $newsletter;
        return $this;
    }

    function getRightLevel_id() {
        return $this->rightLevel_id;
    }
    function setRightLevel_id($rightLevel_id) {
        $this->rightLevel_id = $rightLevel_id;
        return $this;
    }

    function getPasswordexpired() {
        return $this->passwordexpired;
    }
    function setPasswordexpired($passwordexpired) {
        $this->passwordexpired = $passwordexpired;
        return $this;
    }

    function getActive() {
        return $this->active;
    }
    function setActive($active) {
        $this->active = $active;
        return $this;
    }

    function getBooks() {
        return $this->books;
    }
    function setBooks($books) {
        $this->books = $books;
        return $this;
    }
    
    
    public function addBook(Book $book)
    {
        if ($this->books->contains($book)) {
            return;
        }
        $this->books->add($book);
    }

    public function removeBook(Book $book)
    {
        if (!$this->books->contains($book)) {
            return;
        }
        $this->books->removeElement($book);
    }

    public function containsBook(Book $book)
    {
        return $this->books->contains($book);
    }
}
