<?php
    /*DEFINE
    
    $public_site : Contenu du site public
    $private_site : Contenu du site privé
    $login : false si non loggé / string username de l'utilisateur
    $title : titre de la page
    
    $notify (array);
    $notify_function;
    $show : bool (true to show)
      
     */
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?= $title ?></title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="css/clean-blog.css" rel="stylesheet">
    <style>
    .modal-logged {
        margin-left: 10% ;
        margin-right: 10% ;
        max-width:none;
    } 
    </style>
  </head>
  <body>
    <!-- Modal -->
    <div class="modal fade" id="adminModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div id="adminModalWindow" class="modal-dialog  <?php if($login !== null) echo "modal-logged"; ?>" role="document">
        <div class="modal-content" id='<?= $privateSiteDiv ?>'>
          <?= $privateSite ?>
        </div>
      </div>
    </div>
    <header class="masthead" style="background-image: url('img/fond.jpg')">
      <div class="overlay">
      </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 mx-auto">
              <div style="position: absolute; width: 100%;">
                <span class='pull-right' style='padding: 20px;'>
                    <button type="button" class="btn btn-secondary" style="opacity: 0.5" data-toggle="modal" data-target="#adminModal" id='connexionButton'>
                        <?= $login === null ? 'Connexion': $login.' <span class="fa fa-user-circle-o"/>'?>
                    </button>
                </span>
            </div>
            <div class="site-heading">
              <h1>Billet simple pour l'Alaska</h1>
              <span class="subheading">Jean Forteroche</span>
            </div>
          </div>
        </div>
      </div>
    </header>
    <div class="container">
      <div class="row">
        <div class="col-md-12 mx-auto"  id='<?= $publicSiteDiv?>'>
            <?= $publicSite ?>
        </div>
      </div>
    </div>
    <hr>
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 mx-auto">
            <p class="copyright text-muted">Copyright &copy; Jean Forteroche, 2017</p>
          </div>
        </div>
      </div>
    </footer>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
<script src="js/bootstrap-notify.min.js"></script>
<script src="js/moment.js"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<script src="js/clean-blog.min.js"></script>
<script src="js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
        <?= $notify_function ?>
function doUpdate(path, params)
        {
            $.post(path+'.json', params, function(result){
                if(result['show'] === 'hide')   $("#adminModal").modal('hide');
                for(var i in result['divRenderer']) $('#'+ result['divRenderer'][i].div).html(result['divRenderer'][i].content)
                for(var i in result['notify']) <?= $notify_function_name ?>(result['notify'][i].message, result['notify'][i].type, result['notify'][i].icone);
                if(result['login'] !== null)
                {
                    $('#connexionButton').html(result['login'] + ' <span class="fa fa-user-circle-o"/>');
                    $('#adminModalWindow').addClass('modal-logged');
                }
                else
                {
                    $('#connexionButton').html('Connexion');
                    $('#adminModalWindow').removeClass('modal-logged');
                }
                if(result['show'] === 'show')   $("#adminModal").modal('show', 'slow');
                if(result['editMode'] === 1)   showEditor();
                $('.datetimepicker').datetimepicker();
                
            },'JSON');
        }
        
        function showEditor()
        {
            tinymce.remove();
            if($('#chapterEditor').length ===  0) return false;
            $('.chapterEditorDiv').fadeIn(function(){tinymce.init({selector:'#chapterEditor'});});
            $('.chapterNoEditorDiv').fadeOut();
        }
         
        function hideEditor()
        {
            if($('#chapterEditor').length ===  0) return false;
            $('.chapterNoEditorDiv').fadeIn();
            $('.chapterEditorDiv').fadeOut();
        }
        
        $(document).ready(function() {
            <?php 
            if($show === 'show') 
                echo '$("#adminModal").modal("show");';
            if($editMode) 
                echo 'showEditor();';
            

            echo implode("\n", $notify);
            ?>
        });
    </script>
  </body>
</html>
