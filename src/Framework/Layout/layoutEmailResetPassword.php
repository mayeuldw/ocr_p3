
<p>Bonjour,</p>

<?php if($created) { ?>
    <p>Bienvenue sur le site Billet simple pour l'Alaska.</p>
    <p>Vos informations de connexion sont les suivants :</p>
    <ul>
        <li>Identifant : <b><?= $email ?></b></li>
        <li>Mot de passe : <b><?= $pwd ?></b></li>
    </ul>
    
<?php } else {?>
    <p>A votre demande votre mot de passe a été reinitialisé. Votre nouveau mot de passe est le suivant :</p>
    <ul>
        <li>Mot de passe : <b><?= $pwd ?></b></li>
    </ul>

<?php } ?>
    <p>Ce mot de passe est temporaire il vous sera demandé de modifier ce dernier lors de votre prochaine connexion au site.</p>
    <p>Cordialement,</p>
    <br>
    <br>
    <p> Ce mail est un mail automatique, aucune réponse ne sera traité.</P>
    <p>-------</p>
    <a href='http://jura.tadeo.fr/billet-simple-pour-alaska/'>http://jura.tadeo.fr/billet-simple-pour-alaska/</a>
