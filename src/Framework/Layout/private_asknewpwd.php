<div class="modal-header">
    <h2 class="modal-title" id="exampleModalLabel">Billet simple pour l'Alaska</h2>
    <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close" title="Fermer"><span aria-hidden="true" class='fa fa-2x fa-times-circle'> </span></button>
</div>
<div class="modal-body">
        <form id="newPwd">
            <label class='control-label'>Login</label>
            <input type='text' name='login' class='form-control'/>
        </form>
</div>
<div class="modal-footer row">
    <button class='btn btn-block btn-primary' type='button' id='doLogInBtn' onclick="doUpdate('<?= $router->getUrl('private.resetPasswordUserSelf')?>', $('#newPwd').serialize())">
            Demander un nouveau mot de passe
        </button>
        <button class='btn btn-block btn-outline-warning'  type='button' onclick="doUpdate('<?= $router->getUrl('private.users')?>', $('#newPwd').serialize())">
            Retour
        </button>
</div>
