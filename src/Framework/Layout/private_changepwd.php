<div class="modal-header">
    <h2 class="modal-title" id="exampleModalLabel">Billet simple pour l'Alaska</h2>
    <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close" title="Fermer"><span aria-hidden="true" class='fa fa-2x fa-times-circle'> </span></button>
</div>
<div class="modal-body">
        <?php if ($expired) {?>
        <div class="alert alert-danger" style="text-align: center" >Votre mot de passe à expiré.</div>
        <?php } ?>
        <form id="newPwd">
            <label class='control-label'>Mot de passe actuel</label>
            <input type='password' name='pwd1' class='form-control'

            <label class='control-label'>Nouveau mot de passe</label>
            <input type='password' name='pwd2' class='form-control'

            <label class='control-label'>Repeter le mot de passe</label>
            <input type='password' name='pwd3' class='form-control' onkeypress="if (event.keyCode === 13) $('#doLogInBtn').trigger('click'); event.preventDefault;"/>
        </form>
</div>
<div class="modal-footer row">
        <button class='btn btn-block btn-primary' id='doLogInBtn' onclick="doUpdate('<?= $router->getUrl('private.doChangePasswordSelf',['user_id'=> $user_id])?>', $('#newPwd').serialize())">
            Changer mon mot de passe
        </button>
        <?php if ($expired) {?>
        <button class='btn btn-block btn-outline-warning' onclick="doUpdate('<?= $router->getUrl('private.doLogOut') ?>', {})">
            Déconnexion
        </button>
        <?php } else { ?>
        <button class='btn btn-block btn-outline-warning' onclick="doUpdate('<?= $router->getUrl('private.users')?>', {})">
            Retour
        </button>
        <?php } ?>
</div>
