<div class="modal-header">
    <h2 class="modal-title" id="exampleModalLabel">Billet simple pour l'Alaska</h2>
    <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close" title="Fermer"><span aria-hidden="true" class='fa fa-2x fa-times-circle'> </span></button>
</div>
<div class="modal-body">
    <label class='control-label'>Login</label>
    <input type='text' id='user_login' class='form-control'/>

    <label class='control-label'>Mot de passe</label>
    <input type='password' id='user_password' class='form-control' onkeypress="if (event.keyCode === 13) $('#doLogInBtn').trigger('click'); event.preventDefault;"/>
    
</div>
<div class="modal-footer">
    <button class='btn btn-block btn-primary' id='doLogInBtn' onclick="doUpdate('doLogIn', {'login':$('#user_login').val(),'pwd':$('#user_password').val()})">
        Se connecter
    </button>
    <button type="button" class="btn btn-link" onclick="doUpdate('askNewPwd', {})">Mot de passe perdu.</button>
</div>




