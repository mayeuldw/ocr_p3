<?php
    $route_chapters = $router->getUrl('private.bookChapters', ['book_id'=>1]);
    
    $route_logOut = $router->getUrl('private.doLogOut');
    $route_users = $router->getUrl('private.users');
    $route_commentaries = $router->getUrl('private.displayAllReportedCOmmentaries');

?>

<div class="modal-header">
    <div class="col-sm-3">
        <button type="button" onclick="doUpdate('<?= $route_chapters?>', {})" class="btn btn-outline-<?= $route_chapters === $route_name ? 'secondary':'primary'?> btn-block">Chapitres</button> 
    </div>
    <div class="col-sm-3">
        <button type="button" onclick="doUpdate('<?= $route_commentaries?>', {})" class="btn btn-outline-<?= $route_commentaries === $route_name ? 'secondary':'primary'?> btn-block">Commentaires</button> 
    </div>
    <div class="col-sm-3">
        <?php if($displayUserBtn) { ?>
        
        <button type="button" onclick="doUpdate('<?= $route_users?>', {})" class="btn btn-outline-<?= $route_users === $route_name ? 'secondary':'primary'?> btn-block">Utilisateurs</button> 
        <?php } ?>
    </div>
    <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close" title="Fermer"><span aria-hidden="true" class='fa fa-2x fa-times-circle'> </span></button>
</div>
<div class="modal-body">

    <?= $content ?>
</div>
<div class="modal-footer">
    <button class='btn btn-outline-dark pull-right' onclick="doUpdate('<?= $router->getUrl('private.changePasswordSelf')?>', {})">
        Modifier mon mot de passe
    </button>
    <button class='btn btn-outline-warning pull-right' onclick="doUpdate('<?= $route_logOut ?>', {})">
        Se déconnecter
    </button>
    <br><br>
    <!--<button type="button" class="btn-link pull-right">Mot de passe perdu.</button>-->
</div>