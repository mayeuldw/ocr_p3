<?php

namespace Framework\Manager;

use Doctrine\ORM\EntityManager;

class Manager
{
    
    private static $em = null;
    public function __construct() {
        if(self::$em === null)
        {
            self::$em = require __DIR__.'/../doctrine_config.php';
        }
    }
    
    public function em() : EntityManager
    {
        return self::$em;
    }
    
    function quote(string $str) : string
    {
        return '"'.htmlspecialchars($str).'"';
    }
}