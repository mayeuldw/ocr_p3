<?php

namespace Framework\Manager;

use Framework\Core\Notify;

class NotifyManager extends Manager
{
    private $js_function_name = 'display_notify';
    
    function newNotify($msg, $type, $ico)
    {
        return new Notify($msg, $type, $ico);
    }
    
    function display_json_notify(Notify $notify)
    {
        return $notify->get();
    }

    function display_web_notify(Notify $notify)  : string
    {
        $data = $notify->get();
        return $this->getFunctionName().'('.$this->quote($data['message']).','.$this->quote($data['type']).','.$this->quote($data['icone']).');';
    }
    
    
    function getFunctionName() : string
    {
        return $this->js_function_name;
    }
    
    function getFunctionJS() : string
    {
        return '    
        function '.$this->getFunctionName().'(msg, type, ico)
        {    
            $.notify({
                icon: ico,
                message: msg,
            },{
                type: type,
        	placement: {
                    from: "top",
                    align: "center"
                },
        	offset: 20,
                z_index: 3031,
                delay: 5000,
            });
        }';
    }
}
