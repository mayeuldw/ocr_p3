<?php
namespace Framework\Manager;

use Framework\Entity\User;
use Framework\Entity\RightLevel;
use Framework\Entity\Book;
use Framework\Entity\Right;


class UserFrameworkRight extends Manager
{
    public function logUser($login, $password = '')
    {
        $users = $this->em()->getRepository('Framework\\Entity\\User')
                ->findBy([
                    'mail'   => $login,
                    'active'     => 1
                ]);
        foreach($users as $user)
        {
            if($password === $user->getPassword()) return $user;
        }
        return null;
    }
    
    public function get($id)
    {
        if($id === null) return null;
        return $this->em()->find('Framework\\Entity\\User', $id);
    }
    
    public function checkRight(User $user, string $rightName)
    {
        $userLvl = $this->em()->getRepository('Framework\\Entity\\RightLevel')
            ->findOneBy([
                'rightLevel_id'   => $user->getRightLevel_id()
            ]);
        if($userLvl === null) return false;
        if($userLvl->getState() == 0) return true;
        if($userLvl->getState() == 2) return false;
        
        $right = $this->em()->getRepository('Framework\\Entity\\Right')
            ->findOneBy([
                'name'          => $rightName,
                'rightLevel_id'   => $user->getRightLevel_id()
            ]);
        return $right !== null;
    }
    
    public function checkBookRight(User $user, Book $book)
    {
        return $user->containsBook($book);
    }
    
    
    
    
}