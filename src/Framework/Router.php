<?php

namespace Framework;

use Psr\Http\Message\ServerRequestInterface;
use Zend\Expressive\Router\FastRouteRouter;
use Zend\Expressive\Router\Route as ZendRoute;
use Framework\Core\Route;

class Router
{
    private $router;
    
    public function __construct() {
        $this->router = new FastRouteRouter();
    }
    public function get(string $path, callable $callable, string $name)
    {
        $this->router->addRoute(new ZendRoute($path.'.json', $callable, ['POST'], $name.'.json'));
        $this->router->addRoute(new ZendRoute($path, $callable, ['GET'], $name));
    }
    
    public function match(ServerRequestInterface $request, string $allternative_path)
    {
        $result = $this->router->match($request);
        
        if($result->isSuccess())
        {
            return new Route(
                $result->getMatchedRouteName(),
                $result->getMatchedMiddleware(), 
                $result->getMatchedParams()
            );
        }
        if($allternative_path !== '') return $this->match(new \GuzzleHttp\Psr7\ServerRequest('GET', $allternative_path), '');  
        return null;
    }
    
    public function matchString($path, $allternative_path)
    {
        return $this->match(new \GuzzleHttp\Psr7\ServerRequest('GET', $path), $allternative_path);
    }
    public function matchStringPOST($path, $allternative_path)
    {
        return $this->match(new \GuzzleHttp\Psr7\ServerRequest('POST', $path), $allternative_path);
    }
    
    
    public function getUrl(string $name, Array $params = []) : string
    {
        return str_replace('/','',$this->router->generateUri($name, $params));
    }
    
    public function makeRoutes($modules, $path)
    {
        foreach($modules as $module)
        {
            $filename = $path.$module.'/routes.json';
            if(!file_exists($filename)) continue;
            
            $json = json_decode(file_get_contents($filename),true);
            if($json === null)
            {
                echo 'Fail Load Route : '.$module;
                continue;
            }
            foreach($json as $item) foreach($item['routes'] as $r)
            {
                $this->get($r['path'], $this->setCallback($item, $r), $r['name']);
            }
        }
    }
    
    public function setCallback($item, $r)
    {
        return function () use ($item, $r){
            return [
                        'controller' => $item['controller'],
                        'nameSpace' =>  $item['nameSpace'],
                        'functionName' => $r['functionName'],
                    ];
        };
    }
}