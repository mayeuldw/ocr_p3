<?php
// bootstrap.php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$isDevMode = true;
$proxyDir = null;
$cache = null;
$useSimpleAnnotationReader = false;


$classLoader = new \Doctrine\Common\ClassLoader('Entities',__DIR__.'/Entity');
$classLoader->register();

// Connexion à la base de données
$dbParams = [
    'driver'   => 'pdo_mysql',
    'host'     => 'localhost',
    'charset'  => 'utf8',
    'user'     => 'ocrbdd_p3',
    'password' => 'm0tdepasse',
    'dbname'   => 'ocrbdd_p3',
];

$config = Setup::createAnnotationMetadataConfiguration(
    [__DIR__.'/Entity'],
    $isDevMode,
    $proxyDir,
    $cache,
    $useSimpleAnnotationReader
);
$config->addEntityNamespace('Entity', 'Framwork\\Entity');
$entityManager = EntityManager::create($dbParams, $config);

return $entityManager;