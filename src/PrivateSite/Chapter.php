<?php
namespace App\PrivateSite;

use Psr\Http\Message\ServerRequestInterface;
use App\PrivateSite\Manager\ChapterManager;
use App\PrivateSite\Manager\BookManager;

use Framework\Entity\Book;

class Chapter extends \App\PublicSite\PublicSite
{
    
    public function rendererAdminDefaultView(ServerRequestInterface $request)
    {       

        $this->displayAllChapters($request);
        $this->publicView();
    }
    
    public function displayAllChapters(ServerRequestInterface $request)
    {

        if(!$this->isAllowed($request, 'SeeChapterList')) return false; 
        $book = (new BookManager())->getBook($request->getAttribute('book_id') ?? 1) ?? (new BookManager())->getBook(1);
        $this->session['PreviousAdminView'] = $this->router->getUrl('private.bookChapters', ['book_id'=>$book->getId()]);
        $chapterManager = new ChapterManager();
        $chapterList = $chapterManager->getAllChapterOfBook($book->getId());
        $this->rendererAdminView(__DIR__.'/Layout/layoutAllChapterOfBook', [
            'book'=>$book, 
            'chapters' => $chapterList ,
            'route_name'=> $this->session['PreviousAdminView'], 
            'router'=>$this->router,
            'right_to_edit'=>$this->bookIsAllowed($book)
            ]);
    }
    
    function createChapter(ServerRequestInterface $request)
    {
        $bookMangager = new BookManager();
        $book = $bookMangager->getBook($request->getAttribute('book_id'));
        if($book === null)                                  return $this->setNotify('Impossible de créer un nouveau chapitre', 'danger', 'fa fa-file-text');
        if(!$this->isAllowed($request, 'CreateChapter'))    return $this->setNotify('Impossible de créer un nouveau chapitre', 'danger', 'fa fa-file-text');
        if(!$this->bookIsAllowed($book))                    return $this->setNotify('Vous ne pouvez pas créer de nouveaux chapitre pour ce récit', 'danger', 'fa fa-file-text');
        $chapterManager = new ChapterManager();
        $chapter = $chapterManager->createChapter(
                $book->getId(),
                $bookMangager->getNextChapterOrder($book),
                $request->getParsedBody()['title']
            );
        $this->setNotify('Chapitre créé.', 'success', 'fa fa-file-text');
        // TODO change json to pain text
        $this->session['currentChapterView'] = $chapter->getId();
        $this->displayAllChapters($request);
        $this->publicView();
        $this->ctrl_data['editMode'] = true;
        $this->ctrl_data['show'] = 'hide';
    }
    
    function updateChapterTitle(ServerRequestInterface $request)
    {
        $chapterMangager = new ChapterManager();
        $chapter = $chapterMangager->getChapter($request->getAttribute('chapter_id'));
        if(!$this->isAllowed($request, 'UpdateChapter'))    return false; 
        if($chapter === null)                               return $this->setNotify('Ce chapitre n`\'existe pas', 'danger', 'fa fa-file-text');
        
        
        $bookMangager = new BookManager();
        $book = $bookMangager->getBook($chapter->getBookId());
        if(!$this->bookIsAllowed($book))                    return $this->setNotify('Vous ne pouvez pas modifier de chapitres pour ce récit', 'danger', 'fa fa-file-text');
        $chapterMangager->updateTitle($chapter,$request->getParsedBody()['title']);
        $this->setNotify('Titre de chapitre modifié.', 'success', 'fa fa-file-text');
        $this->displayAllChapters($request);
    }
    
    function updateChapterState(ServerRequestInterface $request)
    {
        $chapterMangager = new ChapterManager();
        $chapter = $chapterMangager->getChapter($request->getAttribute('chapter_id'));
        if(!$this->isAllowed($request, 'UpdateChapter'))    return false; 
        if($chapter === null)                               return $this->setNotify('Ce chapitre n`\'existe pas', 'danger', 'fa fa-file-text');
        
        
        $bookMangager = new BookManager();
        $book = $bookMangager->getBook($chapter->getBookId());
        if(!$this->bookIsAllowed($book))                    return $this->setNotify('Vous ne pouvez pas modifier de chapitres pour ce récit', 'danger', 'fa fa-file-text');
        $chapterMangager->updatePublicState($chapter,$request->getParsedBody()['state'], $request->getParsedBody()['date']);
        $this->setNotify('Chapitre modifié.', 'success', 'fa fa-file-text');
        $this->displayAllChapters($request);
        $this->publicView();
    }
    
    function updateChapterContent(ServerRequestInterface $request)
    {
        $chapterMangager = new ChapterManager();
        $chapter = $chapterMangager->getChapter($request->getAttribute('chapter_id'));
        if(!$this->isAllowed($request, 'UpdateChapter'))    return false; 
        if($chapter === null)                               return $this->setNotify('Ce chapitre n`\'existe pas', 'danger', 'fa fa-file-text');
        
        
        $bookMangager = new BookManager();
        $book = $bookMangager->getBook($chapter->getBookId());
        if(!$this->bookIsAllowed($book))                    return $this->setNotify('Vous ne pouvez pas modifier de chapitres pour ce récit', 'danger', 'fa fa-file-text');


        $chapterMangager->updateContent($chapter,$request->getParsedBody()['content'], $this->ctrl_data['user']);
        $this->setNotify('Chapitre modifié.', 'success', 'fa fa-file-text');
        $this->publicView();
    }
    
    function displayChapterEditor(ServerRequestInterface $request)
    {
        $chapterMangager = new ChapterManager();
        $chapter = $chapterMangager->getChapter($request->getAttribute('chapter_id'));

        if(!$this->isAllowed($request, 'UpdateChapter'))    return false; 
        if($chapter === null)                               return $this->setNotify('Ce chapitre n`\'existe pas', 'danger', 'fa fa-file-text');

        $this->setPublicViewById($chapter->getId());
        $this->publicView();
        $this->ctrl_data['editMode'] = true;
        $this->ctrl_data['show'] = 'hide';
    }
    
}
