<?php

namespace App\PrivateSite;


use Psr\Http\Message\ServerRequestInterface;
use App\PrivateSite\Manager\ChapterManager;
use App\PrivateSite\Manager\CommentaryManager;

class Commentary extends \App\PublicSite\PublicSite
{
    public function newCommentary(ServerRequestInterface $request)
    {
        $chapterMangager = new ChapterManager();
        $chapter = $chapterMangager->getChapter($request->getAttribute('chapter_id'));
        if($chapter === null)                               return $this->setNotify('Impossible de créer un commentaire pour ce capitre', 'danger', 'fa fa-file-text');
        
        $commentaryManager = new CommentaryManager();
        $commentaryManager->createCommentary(
                $request->getAttribute('chapter_id'), 
                $request->getAttribute('responseTo'), 
                $request->getParsedBody()['login'], 
                $request->getParsedBody()['title'], 
                $request->getParsedBody()['content']
                );
        $this->setNotify('Votre commentaire à été créé.', 'success', 'fa fa-file-text');
        if($this->isLogged($request))
            $this->publicView();
        else 
            $this->publicView();
        
    }
    
    public function reportCommentary(ServerRequestInterface $request)
    {
        $commentaryManager = new CommentaryManager();
        $commentaryManager->reportCommentary($request->getAttribute('chapterCommentary_id'));
        $this->setNotify('Ce commentaire à été signalé.', 'success', 'fa fa-file-text');
        if($this->isLogged($request))
            $this->publicView();
    }
    
    public function deleteCommentary(ServerRequestInterface $request)
    {
        if(!$this->isAllowed($request, 'deleteCommentary')) return false; 
        $commentaryManager = new CommentaryManager();
        $commentaryManager->deleteCommentary($request->getAttribute('chapterCommentary_id'), $this->ctrl_data['user']->getId());
        $this->setNotify('Ce commentaire à été supprimé.', 'success', 'fa fa-file-text');
        $this->publicView();
        
    }
    
    public function moderateCommentary(ServerRequestInterface $request)
    {
        if(!$this->isAllowed($request, 'moderateCommentary')) return false; 
        $commentaryManager = new CommentaryManager();
        $commentaryManager->moderateCommentary(
                $request->getAttribute('chapterCommentary_id'),
                $this->ctrl_data['user']->getId(),
                $request->getParsedBody()['login'], 
                $request->getParsedBody()['title'], 
                $request->getParsedBody()['content']
                );
        $this->setNotify('Ce commentaire à été moderé.', 'success', 'fa fa-file-text');
        $this->publicView();
    }
    
    public function deleteCommentaryFromAdminView(ServerRequestInterface $request)
    {
        if(!$this->isAllowed($request, 'deleteCommentary')) return false; 
        $commentaryManager = new CommentaryManager();
        $commentaryManager->deleteCommentary($request->getAttribute('chapterCommentary_id'), $this->ctrl_data['user']->getId());
        $this->setNotify('Ce commentaire à été supprimé.', 'success', 'fa fa-file-text');
        $this->publicView();
        $this->displayAllReportedCOmmentaries($request);
        
    }
    
    public function moderateCommentaryFromAdminView(ServerRequestInterface $request)
    {
        if(!$this->isAllowed($request, 'deleteCommentary')) return false; 
        $commentaryManager = new CommentaryManager();
        $commentaryManager->moderateCommentary(
                $request->getAttribute('chapterCommentary_id'),
                $this->ctrl_data['user']->getId(),
                $request->getParsedBody()['login'], 
                $request->getParsedBody()['title'], 
                $request->getParsedBody()['content']
                );
        $this->setNotify('Ce commentaire à été moderé.', 'success', 'fa fa-file-text');
        $this->publicView();
        $this->displayAllReportedCOmmentaries($request);
    }
    
    function displayAllReportedCOmmentaries(ServerRequestInterface $request)
    {
        if(!$this->isAllowed($request, 'seeCommentary')) return false; 
        $commentaryManager = new CommentaryManager();
        $this->session['PreviousAdminView'] = $this->router->getUrl('private.displayAllReportedCOmmentaries');
        $this->rendererAdminView(__DIR__.'/Layout/layoutCommentaries', [
            'commentary'    => $commentaryManager->getReportedCommentaries(),
            'router'        => $this->router,
            'route_name'    => $this->session['PreviousAdminView']
        ]);
   }
   
   function seeInContext(ServerRequestInterface $request)
   {
        $commentaryManager = new CommentaryManager();
        $commentary = $commentaryManager->get($request->getAttribute('chapterCommentary_id'));
        if($commentary === null) return $this->setNotify('Ce commentaire n\existe pas.', 'danger', 'fa fa-file-text');
        
        
        
   }
}
