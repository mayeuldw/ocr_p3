<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$time = time();
?>
<h3><?= $book->getName()?></h3>
<?php if($right_to_edit) {?>
<h4>Nouveau chapitre</h4>
<form id="newChapter">
<div class="col-sm-4 input-group">
    <input type="text" class="form-control" name="title" placeholder="">
    <span class="input-group-btn">
        <button class="btn btn-outline-dark" type="button" onclick="doUpdate('<?= $router->getUrl('private.createChapter', ['book_id'=>$book->getId()]) ?>', $('#newChapter').serialize());" type="button">Créer</button>
    </span>
</div>
</form>
<br>
<?php } ?>

<h4>Liste des chapitres</h4>
<table class='table table-striped'>
    <thead>
        <tr>
            <th>#</th>
            <th>Chapitre</th>
            <th>Visible</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($chapters as $chapter) {?>
        <tr>
            <td><?= $chapter->getOrder() ?></td>
            <td><?= $chapter->getTitle() ?></td>
            <td><?= 
                $chapter->getPublicState() == 0 ? '<span class="badge badge-danger">Non</span>':
                ($chapter->getPublicState() == 1 || strtotime($chapter->getPublicationDate()) < $time ? '<span class="badge badge-success">Oui</span>':
                        '<span class="badge badge-warning">Publication Programmée : '.$chapter->getPublicationDate().'</span>')
            ?></td>
            <?php if($right_to_edit) {?>
            <td><span class='text-info fa fa-chevron-circle-down' onclick="$(this).toggleClass('fa-chevron-circle-down fa-chevron-up'); $('#privateViewChapter_<?= $chapter->getId() ?>').toggle();"/></td>
            <?php } else {?>
            <td></td>
            <?php } ?>

        </tr>
        <?php if($right_to_edit) {?>
        <tr id='privateViewChapter_<?= $chapter->getId() ?>' style='display:none;'>
            <td></td>
            <td style="max-width: 30%">
                <label>Modifer le titre</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="chapterTitle_<?= $chapter->getId() ?>" value="<?= $chapter->getTitle()?>">
                    <span class="input-group-btn">
                        <button class="btn btn-outline-dark" onclick="doUpdate('<?= $router->getUrl('private.updateChapterTitle', ['chapter_id'=>$chapter->getId()]) ?>', {title: $('#chapterTitle_<?= $chapter->getId() ?>').val()});" type="button">Modifier</button>
                    </span>
                </div>
            </td>
            <td style="max-width: 30%">
                <label>Gestion de la publication</label>
                <div class="input-group"> 
                    <select class="form-control" style="height: auto!important;" id="chapterState_<?= $chapter->getId() ?>" onchange="if($(this).val() === '2') $('#chapterDate_<?= $chapter->getId() ?>').fadeIn(); else $('#chapterDate_<?= $chapter->getId() ?>').fadeOut();">
                        <option <?= $chapter->getPublicState() == 0 ? 'selected':''?> value="0">Non public</option>
                        <option <?= $chapter->getPublicState() == 1 ? 'selected':''?> value="1">Public</option>
                        <!--<option <?= $chapter->getPublicState() == 2 ? 'selected':''?> value="2">Publication programmée</option>-->
                    </select> 
                    <input type="text" class="form-control datetimepicker" id="chapterDate_<?= $chapter->getId() ?>" style="display:none;" placeholder="Test"/>
                    <span class="input-group-btn">
                        <button class="btn btn-outline-dark" onclick="doUpdate('<?= $router->getUrl('private.updateChapterState', ['chapter_id'=>$chapter->getId()]) ?>', {state: $('#chapterState_<?= $chapter->getId() ?>').val(),date: $('#chapterDate_<?= $chapter->getId() ?>').val()});" type="button">Modifier</button>
                    </span>
                </div>
            </td>
            <td style="max-width: 30%">
                <label>Modifier le texte</label>
                <button class="btn btn-block btn-outline-dark" onclick="doUpdate('<?= $router->getUrl('private.displayChapterEditor', ['chapter_id'=>$chapter->getId()]) ?>',{});">Modifier le texte</button>
            </td>
        </tr>
        <?php } ?>
<?php } ?>
        
    </tbody>
</table>
