<?php
if(!function_exists('createForm')) {
function createForm($login = '', $title = '', $content = '')
{
    ?>
    <div class="row">
            <div class="col-sm-3">
                <label class="control-label">Nom</label>
            </div>
            <div class="col-sm-9" style="padding-bottom:3px">
                <input type="text" name="login" class="form-control" value="<?= $login ?>"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <label class="control-label">Titre</label>
            </div>
            <div class="col-sm-9" style="padding-bottom:3px">
                <input type="text" name="title" class="form-control" value="<?= $title ?>"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <label class="control-label">Contenu</label>
            </div>
            <div class="col-sm-9" style="padding-bottom:3px">
                <textarea type="text" name="content" rows=3 class="form-control"><?= $content ?></textarea>
            </div>
        </div>
    <?php
}
}

?>



<h3>Commentaires signalés</h3>

<?php 
    $noCommentary = true;
    foreach($commentary as $c) {
        if(!isset($c)) continue;
        $noCommentary = false;
?>
<div>
    <div>
        <div class="commentary_main_div_<?= $c->getId()?>">
            <span style="font-weight: bold"><?= $c->getTitle()?></span> (@<?= $c->getLogin()?>)
            <button class="badge btn-info" ><span class="fa fa-edit" onclick="$('.commentary_main_div_<?= $c->getId()?>').toggle();"> Moderer</span></button>
            <pre><?= $c->getContent()?></pre>
        </div>
        <div class="commentary_main_div_<?= $c->getId()?>" style="display: none;">
            <form id="commentaryModForm_<?= $c->getId()?>" style="border-left : solid black 1px; padding-left : 15px">
                <?php createForm($c->getLogin(), $c->getTitle(), $c->getContent()); ?>
                <div class="row">
                    <div class="col-sm-3">
                        <button 
                            type="button"  style="padding:1px"
                            class="btn btn-outline-warning btn-block" 
                            onclick="doUpdate('<?= $router->getUrl('private.deleteCommentaryFromAdminView', ['chapterCommentary_id'=>$c->getId()]) ?>', {});"
                        >Supprimer</button>
                    </div>
                    <div class="col-sm-6" style="padding-bottom:3px">
                        <button 
                            type="button"  style="padding:1px"
                            class="btn btn-outline-primary  btn-block" 
                            onclick="doUpdate('<?= $router->getUrl('private.moderateCommentaryFromAdminView', ['chapterCommentary_id'=>$c->getId()]) ?>', $('#commentaryModForm_<?= $c->getId()?>').serialize());"
                        >Moderer</button>
                    </div>
                    <div class="col-sm-3" style="padding-bottom:3px">
                        <a 
                            style="padding:1px"
                            class="btn btn-outline-dark btn-block" 
                            href="<?= $this->router->getUrl('public.set', ['chapter_id'=>$c->getChapterId(), 'slug'=>'moderation-de-commentaire-'.date('YmdHis')]) ?>#commentary_main_div_<?= $c->getId()?>" 
                        >Voir dans son contexte</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php } ?>
<?php if($noCommentary) { ?>
<div class="alert alert-dark" style="text-align: center" >Aucun commentaires signalé.</div>
<?php } ?>