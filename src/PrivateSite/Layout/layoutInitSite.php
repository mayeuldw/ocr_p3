
<h1>Compte Administrateur</h1>
<br>
<form id="newUser">
        <div id="new_user" class="row">
            <div class="offset-sm-2 col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon">Nom</span>
                    <input type="text" class="form-control" name="name" value="">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Prénom</span>
                    <input type="text" class="form-control" name="firstName" value="">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Mail</span>
                    <input type="text" class="form-control" name="email" value="">
                </div> 
            </div>
            <div class="col-sm-4">
                <button class="btn btn-block btn-sm btn-primary" type='button' onclick="doUpdate('doInitSite', $('#newUser').serialize());"
                        >initialiser le site</button>
            </div>
        </div>
    </form>