<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    $rCreate = $router->getUrl('private.createUser');
//>
if($seeAllUsers) // is Admin
{
?>
<h3>Utilisateurs</h3>

<h4>Créer un nouvel utilisateur <span class='text-info fa fa-chevron-circle-down' onclick="$(this).toggleClass('fa-chevron-circle-down fa-chevron-up'); $('#new_user').toggle();"/></h4>
    <form id="newUser">
        <div id="new_user" class="row" style="display: none;">
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon">Nom</span>
                    <input type="text" class="form-control" name="name" value="">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Prénom</span>
                    <input type="text" class="form-control" name="firstName" value="">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Mail</span>
                    <input type="text" class="form-control" name="email" value="">
                </div> 
            </div>
            <div class="col-sm-4">
                <div class="input-group"> 
                    <span class="input-group-addon">Niveau de droit</span>
                    <select class="form-control" name="rightLevel" style="height: auto!important;">
                        <?php
                            foreach($rights as $k=>$v)
                                echo '<option value="'.$k.'">'.$v.'</option>';
                        ?>
                    </select> 
                </div>
                <div class="input-group"> 
                    <span class="input-group-addon">Utilisateur</span>
                    <select class="form-control" name="state" style="height: auto!important;">
                        <option value="1" >Actif</option>
                        <option value="0" >Inactif</option>
                    </select> 
                </div>
            </div>
            <div class="col-sm-4">
                <button class="btn btn-block btn-sm btn-primary" type='button' onclick="doUpdate('<?= $rCreate ?>', $('#newUser').serialize());"
                        >Créer cet utilisateur</button>
            </div>
        </div>
    </form>
<br>


<h4>Liste des utilisateurs</h4>
<table class='table table-striped'>
    <thead>
        <tr>
            <th>Nom</th>
            <th>Niveau</th>
            <th>Actif</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($users as $user) {?>
        <tr>
            <td><?= $user->getName() ?>, <?= $user->getFirstName() ?></td>
            <td><?= $rights[$user->getRightLevel_id()] ?></td>
            <td><?= $user->getActive() == 1 ? '<span class="badge badge-primary">Actif</span>':'<span class="badge badge-danger">Inactif</span>'?></td>
            <td><span class='text-info fa fa-chevron-circle-down' onclick="$(this).toggleClass('fa-chevron-circle-down fa-chevron-up'); $('#privateViewUser_<?= $user->getId() ?>').toggle();"/></td>
        </tr>
            
        <tr id='privateViewUser_<?= $user->getId() ?>' style='display:none;'>
            <td  colspan="4">
                <form id="modUser_<?= $user->getId()?>">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="input-group">
                                <span class="input-group-addon">Nom</span>
                                <input type="text" class="form-control" name="name" value="<?= $user->getName()?>">
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">Prénom</span>
                                <input type="text" class="form-control" name="firstName" value="<?= $user->getFirstName()?>">
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">Mail</span>
                                <input type="text" class="form-control" name="email" value="<?= $user->getMail()?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group"> 
                                <span class="input-group-addon">Niveau de droit</span>
                                <select class="form-control" name="rightLevel" style="height: auto!important;">
                                <?php
                                    foreach($rights as $k=>$v)
                                        echo '<option value="'.$k.'" '.($user->getRightLevel_id() === $k ? 'selected':'').'>'.$v.'</option>';
                                ?>
                                </select> 
                            </div>
                            <div class="input-group"> 
                                <span class="input-group-addon">Utilisateur</span>
                                <select class="form-control" name="state" style="height: auto!important;">
                                    <option value="1" <?= $user->getActive() == 1 ? 'selected':''?>>Actif</option>
                                    <option value="0" <?= $user->getActive() == 0 ? 'selected':''?>>Inactif</option>
                                </select> 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <button type="button" class="btn btn-sm btn-block btn-outline-dark" type='button' onclick="doUpdate('<?= $router->getUrl('private.updateUser', ['user_id'=>$user->getId()]) ?>', $('#modUser_<?= $user->getId() ?>').serialize());">Sauvegarder les modifications</button>
                            <button type="button" class="btn btn-sm btn-block btn-light"  type='button' onclick="doUpdate('<?= $router->getUrl('private.resetPasswordUser', ['user_id'=>$user->getId()]) ?>', {});" >Re-initialiser le mot de passe</button>
                        </div>
                    </div>
                </form>
            </td>
        </tr>
<?php } ?>
    </tbody>
</table>

<?php } else { // Afficher juste lui?>
Juste moi!
<?php } ?>
