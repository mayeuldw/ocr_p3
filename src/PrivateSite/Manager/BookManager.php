<?php
namespace App\PrivateSite\Manager;

use Framework\Entity\Book;
use Framework\Entity\Chapter;

class BookManager extends \Framework\Manager\Manager
{
    
    
    function getBook($book_id)
    {
        return $this->em()->find('Framework\\Entity\\Book', $book_id);
    }
    
    function getNextChapterOrder(Book $book)
    {
        $lastChapter = $this->em()->getRepository('Framework\\Entity\\Chapter')
                ->findOneBy([
                    'book_id'   => $book->getId()
                ],[
                    'order'     => 'desc'
                ]);
        if($lastChapter === null) return 1;
        else return $lastChapter->getOrder() + 1;
    }
}