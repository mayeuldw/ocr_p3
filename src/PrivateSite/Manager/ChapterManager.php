<?php
namespace App\PrivateSite\Manager;

use Framework\Entity\Chapter;
use Framework\Entity\ChapterText;
use Framework\Entity\User;

class ChapterManager extends \Framework\Manager\Manager
{
    
    
    function getAllChapterOfBook($book_id)
    {
        return $this->em()->getRepository('Framework\\Entity\\Chapter')
                ->findBy([
                    'book_id'   => $book_id,
                ],[
                    'order'     => 'desc'
                ]);
    }
    
    function getChapter($chapter_id)
    {
        return $this->em()->find('Framework\\Entity\\Chapter', $chapter_id);
    }
    
    function createChapter(int $book_id, int $order, string $title)
    {
        $chapter = new Chapter();
        $chapter->setBookId($book_id);
        $chapter->setOrder($order);
        $chapter->setTitle($title);
        $this->em()->persist($chapter);
        $this->em()->flush();
        return $chapter;
    }
    
    function updateTitle($chapter, $title)
    {
        $chapter->setTitle($title);
        $this->em()->persist($chapter);
        $this->em()->flush();
    }
    
    function updatePublicState(Chapter $chapter, $state, $date)
    {
        $chapter->setPublicState($state);
        //$chapter->setPublicationDate($date);
        $this->em()->persist($chapter);
        $this->em()->flush();
    }
    
    function updateContent(Chapter $chapter, $content, User $user)
    {
        $oldChapterTexts = $this->em()->getRepository('Framework\\Entity\\ChapterText')
                ->findBy([
                    'chapter_id'    => $chapter->getId(),
                    'active'        => 1
                ]);
        $chapterText = new ChapterText();
        $chapterText->setChapter_id($chapter->getId());
        $chapterText->setLogtime(date('Y-m-d H:i:s'));
        $chapterText->setContent($content);
        $chapterText->setUser_id($user->getId());
        $this->em()->persist($chapterText);
        foreach($oldChapterTexts as $oldChapterText)
        {
            $oldChapterText->setActive(0);
            $this->em()->persist($oldChapterText);
        }
        $this->em()->persist($chapter);
        $this->em()->flush();
    }
}