<?php
namespace App\PrivateSite\Manager;

use Framework\Entity\ChapterCommentary;

class CommentaryManager extends \Framework\Manager\Manager
{
    function createCommentary(int $chapter_id, int $responseTo, string $login, string $title, string $content)
    {
        $commentary = new ChapterCommentary();
        $commentary->setChapterId($chapter_id);
        $commentary->setResponseTo($responseTo);
        $commentary->setLogtime(date('Y-m-d H:i:s'));
        $commentary->setLogin($login);
        $commentary->setTitle($title);
        $commentary->setContent($content);
        $this->em()->persist($commentary);
        $this->em()->flush();
        return $commentary;
    }
    
    function reportCommentary(int $chapterCommentary_id)
    {
        $commentary =  $this->em()->find('Framework\\Entity\\ChapterCommentary', $chapterCommentary_id);
        if($commentary === null) return false;
        $commentary->setState(2);
        $this->em()->persist($commentary);
        $this->em()->flush();
        return true;
    }
    
    function deleteCommentary(int $chapterCommentary_id, int $user_id)
    {
        $commentary =  $this->em()->find('Framework\\Entity\\ChapterCommentary', $chapterCommentary_id);
        if($commentary === null) return false;
        $commentary->setState(0);
        $commentary->setUserId($user_id);
        $this->em()->persist($commentary);
        $this->em()->flush();
        return true;
    }
    
    function moderateCommentary(int $chapterCommentary_id, int $user_id, string $login, string $title, string $content)
    {
        $commentary =  $this->em()->find('Framework\\Entity\\ChapterCommentary', $chapterCommentary_id);
        if($commentary === null) return false;
        $commentary->setState(3);
        $commentary->setUserId($user_id);
        $commentary->setLogin($login);
        $commentary->setTitle($title);
        $commentary->setContent($content);
        $this->em()->persist($commentary);
        $this->em()->flush();
        return true;
    }
    
    function getReportedCommentaries()
    {
        return $this->em()->getRepository('Framework\\Entity\\ChapterCommentary')
                ->findBy([
                    'state'   => 2,
                ],[
                    'logtime'     => 'asc'
                ]);
    }
    
    
}