<?php
namespace App\PrivateSite\Manager;


use Framework\Entity\Right;

class RightManager extends \Framework\Manager\Manager
{
    protected $data = [
      ['rightLevel_id'=> 0, 'name'=>'updateRight'],
      ['rightLevel_id'=> 0, 'name'=>'CreateUser'],
      ['rightLevel_id'=> 0, 'name'=>'UpdateUser'],
      ['rightLevel_id'=> 0, 'name'=>'ResetPasswordUser'],
      ['rightLevel_id'=> 1, 'name'=>'SeeChapterList'],
      ['rightLevel_id'=> 1, 'name'=>'UpdateChapter'],
      ['rightLevel_id'=> 2, 'name'=>'deleteCommentary'],
      ['rightLevel_id'=> 2, 'name'=>'moderateCommentary'],
      ['rightLevel_id'=> 1, 'name'=>'seeCommentary'],
      ['rightLevel_id'=> 2, 'name'=>'seeCommentary'],
      ['rightLevel_id'=> 0, 'name'=>'seeUser'],
    ];
    
    public function load()
    {
        $connection = $this->em()->getConnection();
        $platform   = $connection->getDatabasePlatform();
        $connection->executeUpdate($platform->getTruncateTableSQL('rights', true));
        
        $data = [];
        foreach($this->data as $k=>$item)
        {
            $data[$k] = new Right();
            $data[$k]->setRightLevel_id($item['rightLevel_id']);
            $data[$k]->setName($item['name']);
            $this->em()->persist($data[$k]);
        }
        $this->em()->flush();
    }    
}