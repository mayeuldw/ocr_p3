<?php
namespace App\PrivateSite\Manager;


use Framework\Entity\Book;
use Framework\Entity\Chapter;
use Framework\Entity\Right;
use Framework\Entity\RightLevel;

class SiteManager extends \Framework\Manager\Manager
{
    protected $data = [
      ['rightLevel_id'=> 1, 'name'=>'updateRight'],
      ['rightLevel_id'=> 1, 'name'=>'CreateUser'],
      ['rightLevel_id'=> 1, 'name'=>'UpdateUser'],
      ['rightLevel_id'=> 1, 'name'=>'ResetPasswordUser'],
      ['rightLevel_id'=> 2, 'name'=>'SeeChapterList'],
      ['rightLevel_id'=> 2, 'name'=>'CreateChapter'],
      ['rightLevel_id'=> 2, 'name'=>'UpdateChapter'],
      ['rightLevel_id'=> 2, 'name'=>'deleteCommentary'],
      ['rightLevel_id'=> 3, 'name'=>'deleteCommentary'],
      ['rightLevel_id'=> 2, 'name'=>'moderateCommentary'],
      ['rightLevel_id'=> 3, 'name'=>'moderateCommentary'],
      ['rightLevel_id'=> 2, 'name'=>'seeCommentary'],
      ['rightLevel_id'=> 3, 'name'=>'seeCommentary'],
      ['rightLevel_id'=> 1, 'name'=>'seeUser'],
    ];
    
    protected $dataRightLvl = [
        ['id'=>1, 'name'=>'Administrateur', 'state'=>0, 'visible'=>true],
        ['id'=>2, 'name'=>'Editeur', 'state'=>1, 'visible'=>true],
        ['id'=>3, 'name'=>'Modérateur', 'state'=>1, 'visible'=>true],
    ];
    
    public function loadSite()
    {
        // TRUNCATE ALL DB
        $connection = $this->em()->getConnection();
        $platform   = $connection->getDatabasePlatform();
        $connection->executeQuery('SET FOREIGN_KEY_CHECKS = 0;');
        $connection->executeUpdate($platform->getTruncateTableSQL('books'));
        $connection->executeUpdate($platform->getTruncateTableSQL('booksUsers'));
        $connection->executeUpdate($platform->getTruncateTableSQL('chapterCommentaries'));
        $connection->executeUpdate($platform->getTruncateTableSQL('chapters'));
        $connection->executeUpdate($platform->getTruncateTableSQL('chapterTexts'));
        $connection->executeUpdate($platform->getTruncateTableSQL('rightLevels'));
        $connection->executeUpdate($platform->getTruncateTableSQL('rights'));
        $connection->executeUpdate($platform->getTruncateTableSQL('users'));
        $connection->executeQuery('SET FOREIGN_KEY_CHECKS = 1;');

        // Create First Book
        $book = new Book();
        $book->setName('Billet simple pour l\'Alaska');
        $this->em()->persist($book);
        $this->em()->flush();
        
        // Create First Chapter
        $chapter = new Chapter();
        $chapter->setBookId($book->getId());
        $chapter->setTitle('Chapitre 1');
        $this->em()->persist($chapter);
        $this->em()->flush();
        
        // Create RightLevel
        $data = [];
        foreach($this->dataRightLvl as $k=>$item)
        {
            $data[$k] = new RightLevel();
            $data[$k]->setId($item['id']);
            $data[$k]->setName($item['name']);
            $data[$k]->setState($item['state']);
            $data[$k]->setVisible($item['visible']);
            $this->em()->persist($data[$k]);
        }
        $this->em()->flush();
        
        // Create Rights
        $this->loadRights();
    }
    
    public function loadRights()
    {
        $connection = $this->em()->getConnection();
        $platform   = $connection->getDatabasePlatform();
        $connection->executeUpdate($platform->getTruncateTableSQL('rights', true));
        
        $data = [];
        foreach($this->data as $k=>$item)
        {
            $data[$k] = new Right();
            $data[$k]->setRightLevel_id($item['rightLevel_id']);
            $data[$k]->setName($item['name']);
            $this->em()->persist($data[$k]);
        }
        $this->em()->flush();
    }    
}