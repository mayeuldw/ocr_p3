<?php
namespace App\PrivateSite\Manager;
use Framework\Entity\User;
use Framework\Entity\Book;

class UserManager extends \Framework\Manager\Manager
{
    
    
    function getUsers()
    {
        return $this->em()->getRepository('Framework\\Entity\\User')
                ->findBy([], ['rightLevel_id' => 'asc', 'name'=>'asc']);
    }
        function getUser($id)
    {
        return $this->em()->find('Framework\\Entity\\User',$id);
    }
    
    function getUserByLogin(string $login)
    {
        return $this->em()->getRepository('Framework\\Entity\\User')
                ->findOneBy(['mail'=> $login]);
    }
    
    function getRights()
    {
        $right = [];
        $rightLevels = $this->em()->getRepository('Framework\\Entity\\RightLevel')
                ->findBy(['visible'=> true], ['rightLevel_id' => 'asc']);
        foreach($rightLevels as $rightLevel)
            $right[$rightLevel->getId()] = $rightLevel->getName();
        return $right;
    }
    
    function createUser(\Framework\Controller\Controller $Ctrl, string $name, string $firstName, string $email, $right, $active) : User
    {
        $user = new User();
        $user->setName($name);
        $user->setFirstName($firstName);
        $user->setMail($email);
        $user->setRightLevel_id($right);
        $user->setActive($active);
        $this->addUsertoBook($user, 1);
        $this->createTempPassword($Ctrl, $user,true);
        $this->em()->persist($user);
        $this->em()->flush();
        return $user;
        
    }
    
    function updateUser( User $user, string $name, string $firstName, string $email, $right, $active) : User
    {
        $user->setName($name);
        $user->setFirstName($firstName);
        $user->setMail($email);
        $user->setRightLevel_id($right);
        $user->setActive($active);
        $this->em()->persist($user);
        $this->em()->flush(); 
        return $user;
    }
    
    function createTempPasswordAndSave(\Framework\Controller\Controller $Ctrl, User $user, $create = false)
    {
        // Create new temp password
        // Set ExpiredPassword to 1
        $this->createTempPassword($Ctrl, $user, $create);
        $this->em()->persist($user);
        $this->em()->flush(); 
        return $user;
    }
    
    function createTempPassword(\Framework\Controller\Controller $Ctrl, User $user, $create = false)
    {
        // Create new temp password
        // Set ExpiredPassword to 1
        
        $pwd = $this->generatePwd();
        $user->setPassword($pwd);
        $user->setPasswordexpired(1);
        $Ctrl->sendMail($user->getMail(), 'Votre compte', 'layoutEmailResetPassword', [
            'pwd'       => $pwd,
            'login'     => $user->getMail(),
            'created'   => $create
        ]);
        return $user;
    }
    
    function addUsertoBook(User $user, $book_id)
    {
        $book = $this->em()->find('Framework\\Entity\\Book',$book_id);
        $user->addBook($book);
        return $user;
    }
    
    function generatePwd($nb = 8)
    {
        $alphabet = "abcdefghijklmonopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        $passphrase = "";
        for($i=0; $i<$nb; ++$i)
        {
            $passphrase .= $alphabet[rand(0, strlen($alphabet)-1)];
        }
        return $passphrase;
    }
    
    function updatePassword(User $user, $old, $new)
    {
        if($old !== $user->getPassword()) return false;
        $user->setPassword($new);
        $user->setPasswordexpired(0);
        $this->em()->persist($user);
        $this->em()->flush(); 
        return true;
        
    }
    
}