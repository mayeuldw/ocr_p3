<?php

namespace App\PrivateSite;


use Psr\Http\Message\ServerRequestInterface;
use App\PrivateSite\Manager\UserManager;
use App\PrivateSite\Manager\SiteManager;

class Users extends \App\PublicSite\PublicSite
{
    
   function displayAllUsers(ServerRequestInterface $request)
   {
        if(!$this->isAllowed($request, 'seeUser')) return false; 
        $userManager = new UserManager();
        $users = $userManager->getUsers();
        $userRight = $userManager->getRights();
        
        $this->session['PreviousAdminView'] = $this->router->getUrl('private.users');
        $this->rendererAdminView(__DIR__.'/Layout/layoutUsers', [
            'users'         =>$users, 
            'rights'        => $userRight,
            'route_name'    => $this->session['PreviousAdminView'], 
            'router'        => $this->router,
            'seeAllUsers'   => $this->checkRight('seeAllUsers')
                ]);
   }
   
   function createUser(ServerRequestInterface $request)
   {
        if(!$this->isAllowed($request, 'CreateUser')) return false; 
        $userManager = new UserManager();
        $user = $userManager->createUser($this,
                $request->getParsedBody()['name'],
                $request->getParsedBody()['firstName'],
                $request->getParsedBody()['email'],
                $request->getParsedBody()['rightLevel'],
                $request->getParsedBody()['state']
            );
        $this->setNotify('Compte utilisateur crée : '.$user->getName().',  '.$user->getFirstName(), 'success', 'fa fa-user');
        return $this->displayAllUsers($request);
   }
    
   function updateUser(ServerRequestInterface $request)
   {
        if(!$this->isAllowed($request, 'UpdateUser')) return false; 
        $userManager = new UserManager();
        $user = $userManager->getUser($request->getAttribute('user_id'));
        if($user === null) return $this->setNotify('Ce compte n\'existe pas.', 'danger', 'fa fa-user');
        if($user->getId() === $this->session['user_id'])
        {
            $user = $userManager->updateUser($user, 
                $request->getParsedBody()['name'],
                $request->getParsedBody()['firstName'],
                $request->getParsedBody()['email'],
                $user->getRightLevel_id(),
                $user->getActive()
            );
            
            if ($user->getRightLevel_id() !== $request->getParsedBody()['rightLevel'])
                $this->setNotify('Vous ne pouvez modifier votre propre niveau de droit.', 'warning', 'fa fa-user');
            if ($user->getActive() !== $request->getParsedBody()['state'])
                $this->setNotify('Vous ne pouvez désactiver votre propre compte.', 'warning', 'fa fa-user');
        }
        else
        {
            $user = $userManager->updateUser($user, 
                $request->getParsedBody()['name'],
                $request->getParsedBody()['firstName'],
                $request->getParsedBody()['email'],
                $request->getParsedBody()['rightLevel'],
                $request->getParsedBody()['state']
            );
            
        }
        $this->setNotify('Compte utilisateur modifié : '.$user->getName().',  '.$user->getFirstName(), 'success', 'fa fa-user');
        // if user === me ? do update right?
        return $this->displayAllUsers($request);
   }
   
   function resetPasswordUser(ServerRequestInterface $request)
   {
        if(!$this->isAllowed($request, 'ResetPasswordUser')) return false; 
        $userManager = new UserManager();
        $user = $userManager->getUser($request->getAttribute('user_id'));
        if($user === null) return $this->setNotify('Ce compte n\'existe pas.', 'danger', 'fa fa-user');
        $userManager->createTempPasswordAndSave($this, $user);
        $this->setNotify('Un mail avec un mot de passe temproraire a été envoyé a l\'utilisateur : '.$user->getName().',  '.$user->getFirstName(), 'success', 'fa fa-user');
   }
   
   function doChangePasswordSelf(ServerRequestInterface $request)
   {
        $userManager = new UserManager();
        $user = $userManager->getUser($request->getAttribute('user_id'));
        if($userManager->updatePassword($user, $request->getParsedBody()['pwd1'], $request->getParsedBody()['pwd2']))
        {
            $this->setNotify('Votre mot de passe a été modifié.', 'success', 'fa fa-user');
            $this->displayAllUsers($request);
        }
        else
            $this->setNotify('Information non valide', 'danger', 'fa fa-user');
        
   }
   
   function resetPasswordUserSelf(ServerRequestInterface $request)
   {
        $userManager = new UserManager();
        $user = $userManager->getUserByLogin($request->getParsedBody()['login']);
        if($user !== null)
            $userManager->createTempPasswordAndSave($this, $user);
       $this->setNotify('Un mail vous a été envoyé avec votre nouveau mot de passe.', 'success', 'fa fa-user');
       
   }
   
   function doInitRight(ServerRequestInterface $request)
   {
        if(!$this->isAllowed($request, 'updateRight')) return false; 
        (new Manager\SiteManager())->loadRights();
        $this->setNotify('Les droits utilisateurs ont été mis a jour', 'success', 'fa fa-user');
        return $this->displayAllUsers($request);
   }
    
   function initSite(ServerRequestInterface $request)
   {
        $userManager = new UserManager();
        if(count($userManager->getUsers()) !== 0)
        {
            $this->setNotify('Vous ne disposez pas des droits necessaire pour effectuer cette action', 'danger', 'fa fa-user');
            return false;
        }
       // si la BDD est vide (user)
        return $this->rendererPublicView (__DIR__.'/Layout/layoutInitSite');
   }
   
   function doInitSite(ServerRequestInterface $request)
   {
       $userManager = new UserManager();
        if(count($userManager->getUsers()) !== 0)
        {
            $this->setNotify('Vous ne disposez pas des droits necessaire pour effectuer cette action', 'danger', 'fa fa-user');
            return false;
        }
       // si la BDD est vide (user)
        $siteManager = new SiteManager();
        $siteManager->loadSite();
        $user = $userManager->createUser($this,
                $request->getParsedBody()['name'],
                $request->getParsedBody()['firstName'],
                $request->getParsedBody()['email'],
                1,
                1
            );
        $this->setNotify('Le site à été initialisé. Votre compte administrateur a été crée. un mot de passe vous a été envoyé par email.');
        $this->showAdmin();
        $this->rendererLoginView();
        $this->ChapterNotFound($request);
        $this->publicView();
   }
    
}
