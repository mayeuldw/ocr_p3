<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if(!function_exists('createForm')) {
function createForm($login = '', $title = '', $content = '')
{
    ?>
    <div class="row">
            <div class="col-sm-3">
                <label class="control-label">Nom</label>
            </div>
            <div class="col-sm-9" style="padding-bottom:3px">
                <input type="text" name="login" class="form-control" value="<?= $login ?>"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <label class="control-label">Titre</label>
            </div>
            <div class="col-sm-9" style="padding-bottom:3px">
                <input type="text" name="title" class="form-control" value="<?= $title ?>"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <label class="control-label">Contenu</label>
            </div>
            <div class="col-sm-9" style="padding-bottom:3px">
                <textarea type="text" name="content" rows=3 class="form-control"><?= $content ?></textarea>
            </div>
        </div>
    <?php
}
}



?>
<div class="chapterNoEditorDiv">
    
<?php if($previous !== null) {?>
    <a href='<?= $previous['url']?>' class='btn btn-outline-primary btn-block' style='background-color: white'><span class='fa fa-lg fa-angle-left'></span> Chapitre précédent : <?= $previous['title']?></a><br>
<?php } ?>
<?php if($rightToEdit) {?>
    <span class="pull-right">
        <a  href='javascript:showEditor();' class='btn btn-outline-primary' style='background-color: white'><span class='fa fa-lg fa-edit'></span> Modifier</a>
    </span>
<?php } ?>
</div>

<h1>[<?= $order ?>] <?= $title ?></h1>


<div class="chapterNoEditorDiv">
    <div  style="text-align: justify">
        <?= $content ?>
    </div>
    <?php if($next !== null) {?>
        <a href='<?= $next['url']?>' class='btn btn-primary btn-block'>Chapitre suivant :  <?= $next['title']?> <span class='fa fa-lg fa-angle-right'></span></a>
    <?php } ?>
    <br>
    <?php 
        $noCommentary = true;
        foreach($commentary as $c) {
            if(!isset($c['commentary'])) continue;
            $noCommentary = false;
        ?>
    <div class="chapterNoEditorDiv">
        <div <?php if($c['commentary']->getState() === 2 && $hasRightToModerate) echo 'style="border : red solid 1px"';?>>
            <div class="commentary_main_div_<?= $c['commentary']->getId()?>" id="commentary_main_div_<?= $c['commentary']->getId()?>">
                <span style="font-weight: bold"><?= $c['commentary']->getTitle()?></span> (@<?= $c['commentary']->getLogin()?>)
                <?php if($c['commentary']->getState() === 1) { ?><button class="badge btn-warning" onclick="doUpdate('<?= $router->getUrl('private.reportCommentary', ['chapterCommentary_id'=>$c['commentary']->getId()]) ?>', {});" title="Signaler ce commentaire"><span class="fa fa-exclamation"> Signaler</span></button><?php }?>
                <?php if($hasRightToModerate){?> <button class="badge btn-info" ><span class="fa fa-edit" onclick="$('.commentary_main_div_<?= $c['commentary']->getId()?>').toggle();"> Moderer</span></button><?php }?>
                <pre><?= $c['commentary']->getContent()?></pre>
            </div>
            <div class="commentary_main_div_<?= $c['commentary']->getId()?>" style="display: none;">
                <form id="commentaryModForm_<?= $c['commentary']->getId()?>" style="border-left : solid black 1px; padding-left : 15px">
                    <?php createForm($c['commentary']->getLogin(), $c['commentary']->getTitle(), $c['commentary']->getContent()); ?>
                        <div class="row">
                            <div class="col-sm-3">
                    <button 
                            type="button"  style="padding:1px"
                            class="btn btn-outline-warning btn-block" 
                            onclick="doUpdate('<?= $router->getUrl('private.deleteCommentary', ['chapterCommentary_id'=>$c['commentary']->getId()]) ?>', {});"
                        >Supprimer</button>
                            </div>
                            <div class="col-sm-9" style="padding-bottom:3px">
                        <button 
                            type="button"  style="padding:1px"
                            class="btn btn-outline-primary  btn-block" 
                            onclick="doUpdate('<?= $router->getUrl('private.moderateCommentary', ['chapterCommentary_id'=>$c['commentary']->getId()]) ?>', $('#commentaryModForm_<?= $c['commentary']->getId()?>').serialize());"
                        >Moderer</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            
        </div>
        <div style="padding-left: 10%">
            <?php foreach($c['children'] as $cc) { ?>
            <div <?php if($cc->getState() === 2 && $hasRightToModerate) echo 'style="border : red solid 1px"';?>>
                <div class="commentary_main_div_<?= $cc->getId()?>" id="commentary_main_div_<?= $cc->getId()?>">
                    <span style="font-weight: bold"><?= $cc->getTitle()?></span> (@<?= $cc->getLogin()?>)
                    <?php if($cc->getState() === 1) { ?><button class="badge btn-warning" onclick="doUpdate('<?= $router->getUrl('private.reportCommentary', ['chapterCommentary_id'=>$cc->getId()]) ?>', {});" title="Signaler ce commentaire"><span class="fa fa-exclamation"> Signaler</span></button><?php }?>
                    <?php if($hasRightToModerate){?> <button class="badge btn-info" ><span class="fa fa-edit" onclick="$('.commentary_main_div_<?= $cc->getId()?>').toggle();"> Moderer</span></button><?php }?>
                    <pre><?= $cc->getContent()?></pre>
                </div>
            </div>
            <div class="commentary_main_div_<?= $cc->getId()?>" style="display: none;">
                <form id="commentaryModForm_<?= $cc->getId()?>" style="border-left : solid black 1px; padding-left : 15px">
                    <?php createForm($cc->getLogin(), $cc->getTitle(), $cc->getContent()); ?>
                    <div class="row">
                        <div class="col-sm-3">
                            <button 
                                type="button"  style="padding:1px"
                                class="btn btn-outline-warning btn-block" 
                                onclick="doUpdate('<?= $router->getUrl('private.deleteCommentary', ['chapterCommentary_id'=>$cc->getId()]) ?>', {});"
                            >Supprimer</button>
                        </div>
                        <div class="col-sm-9" style="padding-bottom:3px">
                            <button 
                            type="button"  style="padding:1px"
                            class="btn btn-outline-primary  btn-block" 
                            onclick="doUpdate('<?= $router->getUrl('private.moderateCommentary', ['chapterCommentary_id'=>$cc->getId()]) ?>', $('#commentaryModForm_<?= $c['commentary']->getId()?>').serialize());"
                            >Moderer</button>
                        </div>
                    </div>
                </form>
            </div>
            
            
        <?php } ?>
            
        </div>
        <div class="commentairy_toggle_response_<?=$c['commentary']->getId()?>">
            <button class="btn-sm btn-outline-primary" onclick="$('.commentairy_toggle_response_<?=$c['commentary']->getId()?>').toggle()">Répondre</button>
        </div>
        <div class="commentairy_toggle_response_<?=$c['commentary']->getId()?>" style="display:none;">
            <form id="capterCommentary_<?=$c['commentary']->getId()?>" style="border-left : solid black 1px; padding-left : 15px">
                    <?php createForm('', 'Re: '.$c['commentary']->getTitle()); ?>
                    <button 
                    type="button" 
                    class="btn btn-outline-primary btn-block" 
                    onclick="doUpdate('<?= $router->getUrl('private.newCommentary', ['chapter_id'=>$chapter_id, 'responseTo' =>$c['commentary']->getId()]) ?>', $('#capterCommentary_<?=$c['commentary']->getId()?>').serialize());"
                >Repondre ce chapitre</button>
            </form>
        </div>
    <?php } ?>
    <?php if($noCommentary) { ?>
    <div class="alert alert-dark" style="text-align: center" >Aucun commentaires. Soyez le premier à réagir sur ce chapitre.</div>
    <?php } ?>
    <form id="newCapterCommentary" style="border-left : solid black 1px; padding-left : 15px">
        <?php createForm(); ?>
        <button 
            type="button" 
            class="btn btn-outline-primary btn-block" 
            onclick="doUpdate('<?= $router->getUrl('private.newCommentary', ['chapter_id'=>$chapter_id, 'responseTo' => 0]) ?>', $('#newCapterCommentary').serialize());"
        >Commenter ce chapitre</button>
    </form>
</div>
<?php if($rightToEdit) {?>
<div class="chapterEditorDiv" style="display: none;">
    <button class="btn btn-outline-primary btn-block" onclick="hideEditor();">Retour</button>
    <br>
    
    <textarea class="form-control" id="chapterEditor"><?= $content ?></textarea>
    <br>
    <button onclick="$('#chapterEditor').val(tinyMCE.get('chapterEditor').getContent());doUpdate('<?= $routeToEdit ?>', {content : tinyMCE.get('chapterEditor').getContent()});" class="btn btn-primary btn-block">Sauvegarder les modifications</button>
</div>
<?php } ?>



