<?php
namespace App\PublicSite\Manager;

use Framework\Manager\Manager;
use Framework\Entity\Chapter;

class ChapterManager extends Manager
{
    
    
    function getLastPublicChapterOfBook(int $book_id)
    {
        $last = $this->em()->getRepository('Framework\\Entity\\Chapter')
                ->findOneBy([
                    'book_id'   => $book_id,
                    'public'    => 1
                ],[
                    'order'     => 'desc'
                ]);
        if($last === null) return null;
        return $last->getId();
    }
    
    function isChapterExist(int $chapter_id)
    {
        $chapter = $this->em()->find('Framework\\Entity\\Chapter', $chapter_id);
        return $chapter !== null;
    }
    
    function getWholeChapter(int $chapter_id)
    {
        $chapter = $this->em()->find('Framework\\Entity\\Chapter', $chapter_id);
        return [
            'chapter_id'    => $chapter->getId(),
            'bookTitle'     => $this->getBookTitleOfChapter($chapter),
            'title'         => $chapter->getTitle(),
            'order'         => $chapter->getOrder(),
            'content'       => $this->getPublicTextOfChapter($chapter),
            'previous'      => $this->getPreviousOfChapter($chapter),
            'next'          => $this->getnextOfChapter($chapter),
            'commentary'    => $this->getAllCommentaryOfChapter($chapter),
        ];
    }
    
    
    protected function getPublicTextOfChapter($chapter)
    {
        $chap = $this->em()->getRepository('Framework\\Entity\\ChapterText')
                ->findOneBy([
                    'chapter_id'   => $chapter->getId(),
                    'active'    => 1
                ], ['logtime' => 'desc']);
        if($chap === null) return '';
        return $chap->getContent();
    }
    
    protected function getAllCommentaryOfChapter($chapter)
    {
        $commentaries = $this->em()->getRepository('Framework\\Entity\\ChapterCommentary')
                ->findBy([
                    'chapter_id'   => $chapter->getId(),
                ],[
                    'logtime'     => 'asc'
                ]);
        $comm = [];
        foreach($commentaries as $commentary)
        {
            if($commentary->getState() < 1) continue;
            if($commentary->getResponseTo() > 0)
                $comm[$commentary->getResponseTo()]['children'][] = $commentary;
            else
                $comm[$commentary->getId()] = [
                    'commentary'    => $commentary,
                    'children'      => [],
                ];
        }
        return $comm;
    }
    protected function getPreviousOfChapter($chapter)
    {
        $chap = $this->em()->getRepository('Framework\\Entity\\Chapter')
                ->findOneBy([
                    'book_id'   => $chapter->getBookId(),
                    'order'     => $chapter->getOrder() - 1,
                    'public'    => 1
                ]);
        if($chap === null) return null;
        return ['id' => $chap->getId(), 'title'=> $chap->getTitle(), 'title_url'=> $this->titleToSlug($chap->getTitle())];
    }
    protected function getnextOfChapter($chapter)
    {
        $chap = $this->em()->getRepository('Framework\\Entity\\Chapter')
                ->findOneBy([
                    'book_id'   => $chapter->getBookId(),
                    'order'     => $chapter->getOrder() + 1,
                    'public'    => 1
                ]);
        if($chap === null) return null;
        return ['id' => $chap->getId(), 'title'=> $chap->getTitle(), 'title_url'=> $this->titleToSlug($chap->getTitle())];
    }
    
    protected function getBookTitleOfChapter($chapter)
    {
        $book = $this->em()->find('Framework\\Entity\\Book', $chapter->getBookId());
        if($book === null) return '[Book]';
        return $book->getName();
    }
    
    protected function titleToSlug($title)
    {
        return preg_replace("/[^A-Za-z0-9-]/", '', str_replace(' ', '-',strtolower($title)));
    }
    
}
