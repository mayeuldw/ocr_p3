<?php

namespace App\PublicSite;

use App\PublicSite\Manager\ChapterManager;
use Psr\Http\Message\ServerRequestInterface;

class PublicSite extends \Framework\Controller\AdminController
{
    
    function setPublicView(ServerRequestInterface $request)
    {
        $chapter_id = $request->getAttribute('chapter_id');
        $this->session['notFound'] = false;
        $chapterM = new ChapterManager();
        if($chapterM->isChapterExist($chapter_id))
            $this->session['currentChapterView'] = $chapter_id;
        else
            $this->ChapterNotFound($request);
    }
    function setPublicViewById(int $chapter_id)
    {
        $this->session['notFound'] = false;
        $chapterM = new ChapterManager();
        if($chapterM->isChapterExist($chapter_id))
            $this->session['currentChapterView'] = $chapter_id;
        else
            $this->ChapterNotFound($request);
    }
    
    function index(ServerRequestInterface $request)
    {
        // get last ID
        $this->session['notFound'] = false;
        $chapterM = new ChapterManager();
        $chapter_id = $chapterM->getLastPublicChapterOfBook(1);
        if($chapter_id === null)
            return $this->ChapterNotFound($request);
        return $this->session['currentChapterView'] = $chapter_id;
    }
    
    function ChapterNotFound(ServerRequestInterface $request)
    {
        $this->session['notFound'] = true;
    }
    
    protected function publicView()
    {
        if($this->session['notFound'] ?? false)
            return $this->rendererPublicView (__DIR__.'/Layout/notFound');
        $chapterM = new ChapterManager();
        $data = $chapterM->getWholeChapter($this->session['currentChapterView'] ?? 1);
        $data['rightToEdit'] = $this->checkRight('UpdateChapter');
        $data['routeToEdit'] = $this->router->getUrl('private.updateChapterContent', ['chapter_id'=>$data['chapter_id']]);
        $data['router']  = $this->router;
        $data['hasRightToModerate'] = $this->checkRight('moderateCommentary');
        if($data['previous'] !== null)
        {
            $data['previous']['url'] = $this->router->getUrl('public.set', ['chapter_id'=>$data['previous']['id'], 'slug'=>$data['previous']['title_url']]);
        }
        if($data['next'] !== null)
        {
            $data['next']['url'] = $this->router->getUrl('public.set', ['chapter_id'=>$data['next']['id'], 'slug'=>$data['next']['title_url']]);
        }

        // TODO add Routes to Previous and Next chapter
        return $this->rendererPublicView(__DIR__.'/Layout/currentChapter', $data);
    }
    
    
}
